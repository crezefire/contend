#include "Bullet.h"
#include <math.h>

Bullet::Bullet()
{
	//load the bullet bitmap from the file
	Sprite::LoadSprite("bullet.bmp");
	Sprite::SetRenderMode(MODE_DEFAULT);

	//assign default values
	velocity.x = velocity.y = 0;
	range = 0;
	distanceTravelled = 0;

	SetRender(false);
};

Bullet::~Bullet()
{

}

void Bullet::Fire(Point from, Point to, Point start)
{
	//if the bullet has not yet been fired
	if(distanceTravelled <= 0)
	{
		//move it to the start point
		Sprite::Move(start.x, start.y);
	
		//then check to see if 
		//the bullet is to be fired to the
		//left or right
		if(to.x > from.x)
		{
			//set the velocity
			velocity.x = 5;
			velocity.y = 0;
		}
		else
		{
			velocity.x = -5;
			velocity.y = 0;
		}

		//render it
		SetRender(true);
		
		//set is so that the bullet doesn't reinitialise
		distanceTravelled = 1;
	}
}

void Bullet::Reset()
{
	//reset all values and make render false
	distanceTravelled = 0;
	velocity.x = velocity.y = 0;
	SetRender(false);
}

void Bullet::Move()
{
	//if bullet is travelling and not hit the range
	if(distanceTravelled > 0 && distanceTravelled < range)
	{
		//increase the amount of distance travelled
		++distanceTravelled;
		
		//move the bullet
		Sprite::MoveDeltaX(velocity.x);
		Sprite::MoveDeltaY(velocity.y);

	}//if bullet has travelled the range
	else if(distanceTravelled >= range)
	{
		//reset the values
		distanceTravelled = 0;
		velocity.x = velocity.y = 0;
		SetRender(false);
	}

}

void Bullet::Render(HDC destHDC,HDC bitmapHDC)
{
	//move then render the bullet
	Move();
	Sprite::Render(destHDC, bitmapHDC);
}