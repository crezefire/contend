#include "Game.h"

//makes a square pattern
//It is used to create the patterns on the background
void GeneratePattern(int innerWidth, int bck[MAX_TILES_X][MAX_TILES_Y], int tileNum)
{
	//start from the inside and go east
	for(int i = innerWidth; i < MAX_TILES_X - innerWidth; ++i)
		bck[i][innerWidth] = tileNum;

	//then go south
	for(int i = innerWidth; i < MAX_TILES_Y - innerWidth; ++i)
		bck[innerWidth][i] = tileNum;

	//then go west
	for(int i = innerWidth; i < MAX_TILES_Y - innerWidth; ++i)
		bck[MAX_TILES_X - innerWidth - 1][i] = tileNum;

	//and back to north to complete the pattern
	for(int i = innerWidth; i < MAX_TILES_X - innerWidth; ++i)
		bck[i][MAX_TILES_Y - innerWidth - 1] = tileNum;
}

Game::Game()
{
	keydown = false;
	victory = false;
	
	ticker = 0;
	gameEndTimer = 0;

	//load the mouse drag rectangle sprite
	mousespace = (HBITMAP)LoadImage(NULL, "MouseSelection.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	
	//load and set the menu splash screen
	menuScreen.LoadSprite("MenuScreen.bmp");
	menuScreen.SetRenderMode(MODE_DEFAULT);
	menuScreen.SetScroll(false);

	//load and set the menu play button
	menuPlay.LoadSprite("MenuButton.bmp");
	menuPlay.SetRenderMode(MODE_DEFAULT);
	menuPlay.SetScroll(false);
	menuPlay.Move(575, 519);

	//load and set the menu exit button
	menuExit.LoadSprite("MenuButton.bmp");
	menuExit.SetRenderMode(MODE_DEFAULT);
	menuExit.SetScroll(false);
	menuExit.Move(575, 591);

	//set end points of mouse drag rectangle
	a.x = a.y = c.x = c.y = 0;

	//temporarily add six different types of tiles
	//these will then be loaded in the background
	Tile temp[6];

	//the array of tiles to be loaded into the background
	int bck[MAX_TILES_X][MAX_TILES_Y] = {0};
	
	//load the different tiles
	temp[0].LoadTile("Tile2.bmp");
	temp[1].LoadTile("Tile1.bmp");
	temp[2].LoadTile("Tile3.bmp");
	temp[3].LoadTile("tile4.bmp");
	temp[4].LoadTile("Tile5.bmp");
	temp[5].LoadTile("spawnTile.bmp");

	//for creating square patterns on the map
	GeneratePattern(1, bck, 1);		//outermost pattern
	GeneratePattern(4, bck, 2);		//inner pattern 1
	GeneratePattern(8, bck, 3);		//inner patter 2
	GeneratePattern(11, bck, 4);	//innermost pattern


	//the spawn area for the user and computer
	bck[0][12] = 5;
	bck[29][12] = 5;

	//now load the tiles and the array into the background class
	mainBackground->LoadTileArray(6, temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
	mainBackground->LoadBackgroundArray(bck);

	//load and set the bitmap for the cursor
	cursor.LoadSprite("cursor3.bmp");
	cursor.SetRenderMode(MODE_TRANS);
	cursor.SetTransColour(RGB(255, 0, 255));
	cursor.SetScroll(false);

	//remove the default windows cursor
	ShowCursor(false);

	//seed the random number generator
	srand((int)time(NULL));

}

Game::~Game()
{
	;
}

void Game::InitialisePlayers()
{
	//First initialise the user's units

	//Randomly add 3 bases to the map
	user.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));
	user.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));
	user.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));

	//and make sure they do not overlap
	user.SetBases();

	//add 3 bases to the map for the computer
	computer.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));
	computer.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));
	computer.AddBase((rand() % (MAP_WIDTH - BASE_WIDTH - 300)) + 150, rand() % (MAP_HEIGHT - BASE_HEIGHT));

	//make sure they do not overlap 
	//then hide them
	computer.SetBases();
	computer.HideBases();

	//add 2 seekers and 1 engineer for 
	//both user and computer
	user.AddSeeker(10, 3000);
	user.AddSeeker(10, 3100);
	user.AddEngineer(10, 3250);

	computer.AddSeeker(7400, 3000);
	computer.AddSeeker(7400, 3100);
	computer.AddEngineer(7400, 3250);
}

void Game::DrawMouseDrag()
{
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, mousespace);
		
	//to check if the mouse is being dragged to the left or right
	if(mouse->GetCurrentX() < mouse->GetOldX())
	{
		//if mouse is being dragged to the left
		a.x = mouse->GetCurrentX();
		c.x = mouse->GetOldX();
	}
	else
	{
		//if mouse is being dragged to the right
		a.x = mouse->GetOldX();
		c.x = mouse->GetCurrentX();
	}

	//checks if the mouse is being dragged up or down
	if(mouse->GetCurrentY() < mouse->GetOldY())
	{
		//mouse is being dragged up
		a.y = mouse->GetCurrentY();
		c.y = mouse->GetOldY();
	}
	else
	{
		//mouse is being dragged down
		a.y = mouse->GetOldY();
		c.y = mouse->GetCurrentY();
	}

	//draw the mouse drag rectangle
	const int w = 3, sW = 10;

	//top
	StretchBlt(backHDC, a.x, a.y, mouse->GetDragWidth(), w, bitmapHDC, 0, 0, sW, sW, SRCCOPY);

	//right
	StretchBlt(backHDC, a.x + mouse->GetDragWidth(), a.y, w, mouse->GetDragHeight(), bitmapHDC, 0, 0, sW, sW, SRCCOPY);

	//bottom
	StretchBlt(backHDC, c.x - mouse->GetDragWidth(), c.y, mouse->GetDragWidth(), w, bitmapHDC, 0, 0, sW, sW, SRCCOPY);

	//left
	StretchBlt(backHDC, a.x, a.y, w, mouse->GetDragHeight(), bitmapHDC, 0, 0, sW, sW, SRCCOPY);

	SelectObject(bitmapHDC, originalBitMap); 
}

BOOL Game::WaitFor(unsigned long delay)
{
	static unsigned long clockStart = 0;
	unsigned long timePassed;
	unsigned long now = timeGetTime();

	timePassed = now - clockStart;
	if (timePassed >  delay)
	{
		clockStart = now;
		return TRUE;
	}
	else
		return FALSE;
}

void Game::SetBuffers(HWND ghwnd)
{
	GetClientRect(ghwnd, &screenRect);	//creates rect based on window client area
	frontHDC = GetDC(ghwnd);			// Initialises front buffer device context (window)
	backHDC = CreateCompatibleDC(frontHDC);// sets up Back DC to be compatible with the front
	bitmapHDC=CreateCompatibleDC(backHDC);
	theOldFrontBitMap = CreateCompatibleBitmap(frontHDC, screenRect.right, 
		screenRect.bottom);		//creates bitmap compatible with the front buffer
    theOldBackBitMap = (HBITMAP)SelectObject(backHDC, theOldFrontBitMap);
								//creates bitmap compatible with the back buffer
	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));	
}

void Game::DisplayFrame()
{
	BitBlt(frontHDC, screenRect.left,screenRect.top, screenRect.right, 
	screenRect.bottom, backHDC, 0, 0, SRCCOPY);
	FillRect(backHDC, &screenRect, (HBRUSH)GetStockObject(0));	
}

void Game::ReleaseResources(HWND ghwnd)
{
	SelectObject(backHDC,theOldBackBitMap);
	DeleteDC(backHDC);
	DeleteDC(bitmapHDC);
	ReleaseDC(ghwnd,frontHDC);
}

void Game::SelectUnits()
{
	//get the width and height from the mouse drag rectangle
	//co - ordinates
	int width = abs(a.x - c.x), height = abs(a.y - c.y);
	
	//select multiple units only for the user
	user.SelectUnits(width, height, a, c);
}

void Game::CheckInterCollisions()
{
	//check collisions between user and computer
	for(int i = 0; i < user.GetTotalUnits(); ++i)
	{
		for(int j = 0; j < computer.GetTotalUnits(); ++j)
		{
			user.CheckCollisions(computer.GetUnit(j), i);
		}
	}

	//check collision between computer and user
	for(int i = 0; i < computer.GetTotalUnits(); ++i)
	{
		for(int j = 0; j < user.GetTotalUnits(); ++j)
		{
			computer.CheckCollisions(user.GetUnit(j), i);
		}
	}
}

void Game::FogOfWar()
{
	//reset the render to false for all units
	computer.ResetRender();
	
	//if a computer unit is detected within 500 pixels
	//of a user unit, it is then revealed
	for(int i = 0; i < user.GetTotalUnits(); ++i)
	{
		for(int j = 0; j < computer.GetTotalUnits(); ++j)
		{
			computer.CheckCollisions(user.GetUnit(i), j, 500);
		}
	}

	//reset the collision for the computer, because
	//no actual collision occurred
	computer.ResetCollisions();
	
}

int Game::GameMenu()
{
	//update the cursor position
	cursor.Move(mouse->GetCurrentX() - 2, mouse->GetCurrentY() - 1);
	
	menuScreen.Render(backHDC, bitmapHDC);
	
	//render the cursor last
	cursor.Render(backHDC, bitmapHDC);

	DisplayFrame();

	//to check if a button has been pressed
	if(mouse->GetMouseButton(LMB) == true)
	{
		//the the user has pressed the play button i.e. the cursor is inside the boundaries of the button
		if(mouse->GetCurrentX() > menuPlay.GetGlobal().x && mouse->GetCurrentX() < (menuPlay.GetGlobal().x + menuPlay.GetWidth() )&&
			mouse->GetCurrentY() > menuPlay.GetGlobal().y && mouse->GetCurrentY() < (menuPlay.GetGlobal().y + menuPlay.GetHeight()))
		{
			//reset the victory and start the games
			victory = false;
			return GAME_RUN;
		}
		
		//the the user has pressed the exit button i.e. the cursor is inside the boundaries of the button, quits the program
		if(mouse->GetCurrentX() > menuExit.GetGlobal().x && mouse->GetCurrentX() < (menuExit.GetGlobal().x + menuExit.GetWidth() )&&
			mouse->GetCurrentY() > menuExit.GetGlobal().y && mouse->GetCurrentY() < (menuExit.GetGlobal().y + menuExit.GetHeight()))
			PostQuitMessage(0);
	}
	
	return GAME_MENU;
}

int Game::GamePause()
{
	//update the cursor position
	cursor.Move(mouse->GetCurrentX() - 2, mouse->GetCurrentY() - 1);
			
	//render main background first
	mainBackground->Render(backHDC, bitmapHDC);
	
	//render all sprites
	user.Render(backHDC, bitmapHDC);
	computer.Render(backHDC, bitmapHDC);
			
	//render the HUD
	mainDisplay.Render(user.currentBase, user.currentlySelected, user.multiple, user.GetMetal(), backHDC, bitmapHDC);
	
	//draw the minimap icons over the HUD
	user.DrawMiniMapFriendly(backHDC, bitmapHDC);
	computer.DrawMiniMapEnemy(backHDC, bitmapHDC);

	//display the pause message
	menuPlay.Render(backHDC, bitmapHDC);
	currentFont->Print("Paused", 595, 539, backHDC, bitmapHDC);

	//render the cursor last
	cursor.Render(backHDC, bitmapHDC);

	//display everything
	DisplayFrame();

	if(keyboard->GetKey('P') && !keydown)
		keydown = true;
	
	if(!keyboard->GetKey('P') && keydown)
	{
		keydown = false;
		return GAME_RUN;
	}

	return GAME_PAUSE;
}

int Game::GameEnd()
{
	//render after a delay of 500
	while(WaitFor(500))
	{	
		//increase the game timer
		++gameEndTimer;
		//if a few seconds have passed
		//reset the timer and go back to the menu
		if(gameEndTimer >= 20)
		{
			gameEndTimer = 0;
			return GAME_MENU;
		}
	}

	//display the victory or defeat message
	if(victory == true)
		currentFont->Print("VICTORY", 620, 340, backHDC, bitmapHDC);
	else
		currentFont->Print("DEFEAT", 620, 340, backHDC, bitmapHDC);
	
	//display the sprites
	DisplayFrame();

	return GAME_END;
}

int Game::GameRun()
{
	//updates certain values only after
	//a short delay
	while(WaitFor(500))
	{
		//adds more metal for the user and computer
		user.ChangeMetal(METAL_INCREMENT);
		computer.ChangeMetal(METAL_INCREMENT);
		
		//increments the timer for the bases
		//to spawn a unit
		user.IncrementTimer();
		computer.IncrementTimer();
	}
	
	//update the cursor position
	cursor.Move(mouse->GetCurrentX() - 2, mouse->GetCurrentY() - 1);
			
	//render main background first
	mainBackground->Render(backHDC, bitmapHDC);
			
	//Only reveal nearby enemy units
	FogOfWar();

	//run the computer AI
	computer.ProcessAI(user.GetBaseVector(), user.GetUnitsVector());
	
	//process the ablities of various units
	user.ProcessPassive(computer.GetBaseVector(), computer.GetUnitsVector());
	user.ProcessActive(computer.GetBaseVector(), computer.GetUnitsVector());

	computer.ProcessPassive(user.GetBaseVector(), user.GetUnitsVector());
	computer.ProcessActive(user.GetBaseVector(), user.GetUnitsVector());

	//check inter collision with bases
	user.CheckCollisionWithBase(computer.GetBaseVector());

	//check for collision between a unit and a base
	user.CheckCollisionWithBase();
	
	//check for collisions between user and comp
	CheckInterCollisions();

	//check for collisions
	user.CheckCollisions();
	computer.CheckCollisions();

	//get the mouse positions and process them
	user.ProcessInput();

	//move the units
	user.MoveUnits();
	computer.MoveUnits();

	//render all sprites
	user.Render(backHDC, bitmapHDC);
	computer.Render(backHDC, bitmapHDC);
			
	//draw mouse drag space
	if(mouse->isDrag())
		DrawMouseDrag();

	//update currently selected units
	user.UpdateSelected();

	//render the HUD
	mainDisplay.Render(user.currentBase, user.currentlySelected, user.multiple, user.GetMetal(), backHDC, bitmapHDC);
	
	//draw the minimap icons over the HUD
	computer.DrawMiniMapEnemy(backHDC, bitmapHDC);
	user.DrawMiniMapFriendly(backHDC, bitmapHDC);

	//render the cursor last
	cursor.Render(backHDC, bitmapHDC);

	//display everything
	DisplayFrame();

	//process the scroll values
	if(keyboard->GetKey(VK_ESCAPE))
	{
		GameReset();
		victory = false;
		return GAME_MENU;
	}

	//scroll the background
	//right
	if(keyboard->GetKey('D'))
		mainBackground->ScrollX(SCROLL_VALUE);

	//left
	if(keyboard->GetKey('A'))
		mainBackground->ScrollX(-1 * SCROLL_VALUE);

	//up
	if(keyboard->GetKey('W'))
		mainBackground->ScrollY(-1 * SCROLL_VALUE);

	//down
	if(keyboard->GetKey('S'))
		mainBackground->ScrollY(SCROLL_VALUE);

	//if player want to pause the game
	//set that P has been pressed
	if(keyboard->GetKey('P') && !keydown)
		keydown = true;
	
	//when P is released
	if(!keyboard->GetKey('P') && keydown)
	{
		//pause the game
		keydown = false;
		return GAME_PAUSE;
	}

	//if the computer has lost all its bases
	//the user wins, else computer wins
	//resets all the values
	//sets the victory and goes to 
	//end of game screen
	if(computer.GetNumberOfBases() <= 0)
	{
		victory = true;
		GameReset();
		return GAME_END;
	}
	else if(user.GetNumberOfBases() <= 0)
	{
		victory = false;
		GameReset();
		return GAME_END;
	}

	return GAME_RUN;
}

void Game::GameReset()
{
	//resets all values set to 
	//default ones
	ticker = 0;
	gameEndTimer = 0;
	a.x = a.y = c.x = c.y = 0;

	//reset and removes all units
	//for computer and user
	computer.ResetPlayer();
	user.ResetPlayer();

	//re initializes the players
	InitialisePlayers();
	
	//resets the background scroll to 0
	mainBackground->Reset();
		
	//no key is being pressed
	keydown = false;
}
