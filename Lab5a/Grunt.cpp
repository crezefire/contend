#include "Grunt.h"

Grunt::Grunt()
{
	Sprite::LoadSprite("Grunt.bmp");
	Sprite::SetTransColour(RGB(0, 0, 0));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(200);
	CSprite::SetFuel(50000);

	currentUnit = -1;
	numberOfUnits = 3;
}

Grunt::~Grunt()
{
	;
}

void Grunt::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	//if the units vector has been changed
	if(numberOfUnits != units.size())
	{
		numberOfUnits = units.size();
		//find a new unit to protect
		currentUnit = -1;
	}
	
	//if a unit has not been found
	if(currentUnit == -1)
	{
		//search for a unit giving priority
		//to certain units
		for(int i = 0; i < units.size(); ++i)
		{
			if(units[i]->GetType() == FIGHTER)
			{
				currentUnit = i;
			}
			else if(units[i]->GetType() == SEEKER)
			{
				currentUnit = i;
			}
			else if(units[i]->GetType() == WIZARD)
			{
				currentUnit = i;
			}
			else if(units[i]->GetType() == STEALER)
			{
				currentUnit = i;
			}
			else
				currentUnit = i;
		}
	}//if unit has been found, move in front of it
	else if(currentUnit != -1)
	{
		Point temp;
		temp.x = units[currentUnit]->GetGlobal().x - 110;
		temp.y = units[currentUnit]->GetGlobal().y;
		this->SetDestination(temp);
	}

}