#include "HUD.h"

#include <stdlib.h>

Hud::Hud()
{
	//load all the bitmaps from the files
	basicOverlay = (HBITMAP)LoadImage(NULL, "HUD.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	miniMap = (HBITMAP)LoadImage(NULL, "black.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	health = (HBITMAP)LoadImage(NULL, "healthbar.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	fuel = (HBITMAP)LoadImage(NULL, "fuelbar.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	region = (HBITMAP)LoadImage(NULL, "minimapRegion.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	BITMAP temp;
	GetObject(basicOverlay, sizeof(temp), &temp);
	
	//get width and height of the basicOverlay
	height = temp.bmHeight;
	width = temp.bmWidth;
}

Hud::~Hud()
{
	;
}

void Hud::Render(Base *base, CSprite *selected, bool multiple, int metal, HDC destHDC, HDC bitmapHDC)
{
	//first print out the amount of metal the user has
	char buffer[20];
	currentFont->Print("METAL: ", 0, 0, destHDC, bitmapHDC);
	currentFont->Print(itoa(metal, buffer, 10), 110, 0, destHDC, bitmapHDC);
	
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, basicOverlay);

	BitBlt(destHDC, 0, 565, 0 + width, 565 + height, bitmapHDC, 0, 0, SRCCOPY);

	//draw background of minimap

	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, miniMap);
	
	StretchBlt(destHDC, 990, 570, 292, 175, bitmapHDC, 0, 0, 4, 2, SRCCOPY);

	//draw screen region on the minimap
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, region);

	StretchBlt(destHDC, 990 + (mainBackground->GetScroll().x / 25), 570 + (mainBackground->GetScroll().y / 45), 51, 1, bitmapHDC, 0, 0, 1, 1, SRCCOPY);

	StretchBlt(destHDC, 990 + (mainBackground->GetScroll().x / 25), 570 + (mainBackground->GetScroll().y / 45), 1, 17, bitmapHDC, 0, 0, 1, 1, SRCCOPY);

	StretchBlt(destHDC, 990 + (mainBackground->GetScroll().x / 25), 570 + (mainBackground->GetScroll().y / 45) + 17, 51, 1, bitmapHDC, 0, 0, 1, 1, SRCCOPY);

	StretchBlt(destHDC, 990 + (mainBackground->GetScroll().x / 25) + 51, 570 + (mainBackground->GetScroll().y / 45), 1, 17, bitmapHDC, 0, 0, 1, 1, SRCCOPY);
	
	//if a unit has been selected
	if(selected != nullptr)
	{
		if(selected != NULL)
		{
			int type = 0;

			//if multiple units have been selected
			if(multiple)
				type = NONE;
			else
				type = selected->GetType();
		
			//print out the type of unit currently selected
			switch(type)
			{
			case SEEKER:
				currentFont->Print("SEEKER", 50, 590, destHDC, bitmapHDC);
				break;

			case STEALER:
				currentFont->Print("STEALER", 50, 590, destHDC, bitmapHDC);
				break;

			case WIZARD:
				currentFont->Print("WIZARD", 50, 590, destHDC, bitmapHDC);
				break;

			case MINER:
				currentFont->Print("MINER", 50, 590, destHDC, bitmapHDC);
				break;

			case FIGHTER:
				currentFont->Print("FIGHTER", 50, 590, destHDC, bitmapHDC);
				break;

			case ENGINEER:
				currentFont->Print("ENGINEER", 50, 590, destHDC, bitmapHDC);
				break;

			case GRUNT:
				currentFont->Print("GRUNT", 50, 590, destHDC, bitmapHDC);
				break;

			case NONE:
				currentFont->Print("MULTIPLE UNITS", 5, 590, destHDC, bitmapHDC);
				break;
			}

			//print out the current health of the unit
			originalBitMap = (HBITMAP)SelectObject(bitmapHDC, health);

			float current = selected->GetHealth(), max = selected->GetMaxHealth();
			//calculate the width of the health bar
			float width = (current / max) ;
			width = width * (480);

			StretchBlt(destHDC, 325, 600, width, 40, bitmapHDC, 0, 0, 4, 2, SRCCOPY);

			currentFont->Print("HEALTH", 687, 604, destHDC, bitmapHDC); 

			//print out the current fuel of the unit
			originalBitMap = (HBITMAP)SelectObject(bitmapHDC, fuel);

			current = selected->GetFuel(), max = selected->GetMaxFuel();
			//calculate the width of the fuel bar
			width = current / max;
			width = width * (480);

			StretchBlt(destHDC, 325, 680, width, 40, bitmapHDC, 0, 0, 4, 2, SRCCOPY);

			currentFont->Print("FUEL", 717, 684, destHDC, bitmapHDC);
		}
	}
	else if(base != nullptr)
	{
		//if a base has been selected
		currentFont->Print("BASE", 50, 590, destHDC, bitmapHDC);

		//print out the type of units the base has
		switch(base->GetType())
		{
		case SEEKER:
			currentFont->Print("Units: Seeker", 5, 610, destHDC, bitmapHDC);
			break;

		case WIZARD:
			currentFont->Print("Units: Wizard", 5, 610, destHDC, bitmapHDC);
			break;

		case FIGHTER:
			currentFont->Print("Units: Fighter", 5, 610, destHDC, bitmapHDC);
			break;

		case ENGINEER:
			currentFont->Print("Units: Engineer", 5, 610, destHDC, bitmapHDC);
			break;

		case GRUNT:
			currentFont->Print("Units: Grunt", 5, 610, destHDC, bitmapHDC);
			break;

		case STEALER:
			currentFont->Print("Units: Stealer", 5, 610, destHDC, bitmapHDC);
			break;

		case MINER:
			currentFont->Print("Units: Miner", 5, 610, destHDC, bitmapHDC);
			break;

		case NONE:
			currentFont->Print("NO UNITS", 15, 620, destHDC, bitmapHDC);
			break;
		}

		//print out the current health of the unit
		originalBitMap = (HBITMAP)SelectObject(bitmapHDC, health);

		float current = base->GetHealth(), max = base->GetMaxHealth();
		//find the width of the healt bar
		float width = (current / max) ;
		width = width * (480);

		StretchBlt(destHDC, 325, 600, width, 40, bitmapHDC, 0, 0, 4, 2, SRCCOPY);

		//print out the text health so that the user knows it is the health bar
		currentFont->Print("HEALTH", 687, 604, destHDC, bitmapHDC); 
	}

	SelectObject(bitmapHDC, originalBitMap);
	
}