#include "Miner.h"

Miner::Miner()
{
	Sprite::LoadSprite("Miner.bmp");
	Sprite::SetTransColour(RGB(0, 0, 0));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(80);
	CSprite::SetFuel(30000);

	closestBase = -1;
	numberOfBases = 3;
}

Miner::~Miner()
{
	;
}

void Miner::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	Point temp;

	//if the number of bases has changed
	if(numberOfBases != base.size())
	{
		//resest this number
		numberOfBases = base.size();
		//find the closest base again
		closestBase = -1;
	}
	
	//if closest base not found
	if(closestBase == -1)
	{
		temp.x = 0;
		temp.y = 0;

		//find it and change it to the index number of the base
		for(int i = 0; i < base.size(); ++i)
			if(temp.x < base[i]->GetGlobal().x)
			{
				temp.x = base[i]->GetGlobal().x;
				closestBase = i;
			}
	}//if unit is at destination, do nothing, let passive take over
	else if(this->isAtDestination() && closestBase != -1)
	{
		;
	}//if closest base found move towards it
	else if(closestBase != -1)
	{
		temp.x = base[closestBase]->GetGlobal().x + 860;
		temp.y = base[closestBase]->GetGlobal().y + 110;
		this->SetDestination(temp);
	}
}