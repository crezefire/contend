#include "Background.h"
#include <Windows.h>
#include <stdio.h>
#include <stdarg.h>

//set the instance as null
Background *Background::instance = nullptr;


Background::Background()
{
	//initialise the tiles to null
	allTiles = NULL;
	scroll.x = 0;
	scroll.y = 0;
}

Background::~Background()
{
	delete[] allTiles;
	delete instance;
}

void Background::LoadTileArray(int _maxTiles, ...)
{
	//get all the tiles passed into the function
	va_list tilesList;

	va_start(tilesList, _maxTiles);

	//allocate memory to the allTiles array
	//based on the total number of tiles passed
	allTiles = new Tile[_maxTiles];

	//add the tiles to the array
	for(int i = 0; i < _maxTiles; ++i)
		allTiles[i] = va_arg(tilesList, Tile);

	va_end(tilesList);
}

void Background::LoadBackgroundArray(int tiles[MAX_TILES_X][MAX_TILES_Y])
{
	//load the background tile array with the index number of the tile
	for(int i = 0; i < MAX_TILES_X; ++i)
		for(int j = 0; j < MAX_TILES_Y; ++j)
			BackgroundTiles[i][j] = tiles[i][j];
}

void Background::ScrollX(int _x)
{
	//scroll in the x direction
	scroll.x += _x;
	
	//check if the values are valid
	if(scroll.x <= 0)
		scroll.x = 0;

	if(scroll.x >= (MAP_WIDTH - SCREEN_WIDTH))
		scroll.x = MAP_WIDTH - SCREEN_WIDTH;
}

void Background::ScrollY(int _y)
{
	//scroll in the y direction
	scroll.y += _y;
	
	//check if the values are valid
	if(scroll.y <= 0)
		scroll.y = 0;

	//175 is added for the HUD display
	if(scroll.y >= (MAP_HEIGHT - SCREEN_HEIGHT + 175))
		scroll.y = MAP_HEIGHT - SCREEN_HEIGHT + 175;
}

void Background::Render(HDC destHDC, HDC bitmapHDC)
{
	int screenx, screeny;
	int sizex, sizey;
	int shiftX, shiftY;
	
	//go through the tile index array
	for (int x = 0; x < MAX_TILES_X; x++)
	{
		for (int y = 0; y < MAX_TILES_Y; y++)
		{
			//get width and height of the current tile
			int tileW = allTiles[BackgroundTiles[x][y]].GetWidth();
			int tileH = allTiles[BackgroundTiles[x][y]].GetHeight();
			
			//figure out if tile is on screen
			if ((((x + 1) * tileW) - scroll.x > 0 
				&& (x * tileW) - scroll.x < SCREEN_WIDTH) &&
				(((y + 1) * tileH) - scroll.y > 0 
				&& (y * tileH) - scroll.y < SCREEN_HEIGHT))
			{
				//figure how much of the tile is on screen
				screenx = (x * tileW) - scroll.x;
				screeny = (y * tileH) - scroll.y;
				if (screenx < 0) 
				{
					//if tile starts off screen screen x will currently be a negative number
					//this changes it so that the off screen part isnt drawn 
					sizex = tileW + screenx;
					screenx = 0;
					shiftX = tileW - sizex;
				}
				//if tile goes passed the screen size change it to stop drawing when it hits the edge
				else if (screenx + tileW > SCREEN_WIDTH) 
				{
					sizex = SCREEN_WIDTH - screenx;
					shiftX = 0;
				}
				//if tile is fully on screen draw it fully
				else
				{
					sizex = tileW;
					shiftX = 0;
				}

				//do the same for the y values
				if (screeny < 0) 
				{
					sizey = tileH + screeny;
					screeny = 0;
					shiftY = tileH - sizey;
				}
				else if (screeny + tileH > SCREEN_HEIGHT) 
				{
					sizey = SCREEN_HEIGHT - screeny;
					shiftY = 0;
				}
				else 
				{
					sizey = tileH;
					shiftY = 0;
				}
				
				//render the tile by pasing it the correct values
				allTiles[BackgroundTiles[x][y]].Render(destHDC, bitmapHDC, screenx, screeny, shiftX, shiftY);
			}
		}
	}
}