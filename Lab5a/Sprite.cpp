#include "Sprite.h"
#include "Background.h"
#include "Input.h"
#include <math.h>

Sprite::Sprite()
{
	//set the values to default
	RenderMode = MODE_DEFAULT;
	render = true;
	
	local.x = local.y = global.x = global.y = spriteSheet.x = spriteSheet.y = 0;
	
	width = 0;
	height = 0;
	
	trans = RGB(255, 255, 255);
	frameNo = 0;
	
	collisionSide = COLLISION_NONE;
	scrolling = true;
	collision.x = collision.y = collisionSize.x = collisionSize.y = 0;

	collisionLeft = collisionRight = collisionTop = collisionBot = false;

	externalCollision = false;
}

Sprite::~Sprite()
{
}

void Sprite::Render(HDC destHDC, HDC bitmapHDC)
{
	//if sprite has to render
	if(render)
	{
		//assuming sprite is on screen
		bool onScreen = true;
		
		//if sprite uses scrolling
		if(scrolling)
		{
			//check to see if sprite is on the screen
			if(global.x > 0 && global.x < mainBackground->GetScroll().x + SCREEN_WIDTH &&
					global.y > 0 && global.y < mainBackground->GetScroll().y + SCREEN_HEIGHT)
			{
				//get the co ordinates
				local.x = global.x - mainBackground->GetScroll().x;
				local.y = global.y - mainBackground->GetScroll().y;
			}
			else
				onScreen = false;
		}
		else
		{
			//if sprite is not scrolling
			local.x = global.x;
			local.y = global.y;
		}

		//only render if sprite is visible on screen
		if(onScreen)
		{
			HBITMAP originalBitMap;
			//switch to the sprite bitmap
			originalBitMap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

			//switch to the render mode
			switch(RenderMode)
			{
				case MODE_DEFAULT: 
					BitBlt(destHDC, local.x, local.y, local.x + width, local.y + height, bitmapHDC, spriteSheet.x, spriteSheet.y, SRCCOPY);
					break;

				case MODE_TRANS:
					TransparentBlt(destHDC, local.x, local.y, width, height, bitmapHDC, spriteSheet.x, spriteSheet.y, width, height, trans);
					break;
			}
	
			SelectObject(bitmapHDC, originalBitMap); 
		}
	}
	
}

bool Sprite::LoadSprite(char *fileName)
{
	//load the sprite bitmap from a bmp file
	bitmap = (HBITMAP)LoadImage(NULL, fileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	if(!bitmap)
		return false;

	BITMAP temp;
	GetObject(bitmap, sizeof(temp), &temp);
	
	//get the width and height of the sprite bitmap
	height = temp.bmHeight;
	width = temp.bmWidth;

	//approximate the radius of the sprite
	if(width > height)
		radius = width / 2;
	else
		radius = height / 2;

	return true;
}

bool Sprite::LoadSprite(char *fileName, int _width, int _height)
{
	//load the sprite bitmap from a bmp file
	bitmap = (HBITMAP)LoadImage(NULL, fileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	if(!bitmap)
		return false;

	BITMAP temp;
	GetObject(bitmap, sizeof(temp), &temp);
	
	//get width and height of the spritesheet
	spriteHeight = temp.bmHeight;
	spriteWidth = temp.bmWidth;

	//set current width and height as the ones passed to the function
	height = _height;
	width = _width;

	//approximate radius
	radius = (width * width) + (height * height);
	radius = sqrt((float)radius);
	radius = radius / 2;

	return true;
}

void Sprite::NextFrame()
{
	//move to the next frame in the sprite sheet
	spriteSheet.x += width;
	
	//increase the frame number
	++frameNo;
	
	//if sprite sheet x value has gone over limit
	//reset it and move down the spritesheet
	if(spriteSheet.x >= spriteWidth)
	{
		spriteSheet.x = 0;
		spriteSheet.y += height;
	}

	//if sprite sheet y values has gone over limit
	//reset it and move to the original position
	if(spriteSheet.y >= spriteHeight)
	{
		spriteSheet.x = 0;
		spriteSheet.y = 0;
		frameNo = 0;
	}

}

void Sprite::PrevFrame()
{
	//move to the previous frame
	//in the spritesheet
	spriteSheet.x -= width;
	
	//decrease frame number
	--frameNo;
	
	//if value is less than zero
	//reset it to the end of the spritesheet
	if(spriteSheet.x < 0)
	{
		spriteSheet.x = spriteWidth - width;
		spriteSheet.y -= height;
	}

	//if value is less than zero
	//reset it to the previous frame in the sprite sheet
	if(spriteSheet.y < 0)
	{
		spriteSheet.x = spriteWidth - width;
		spriteSheet.y = spriteHeight - height;
		frameNo = (spriteWidth * spriteHeight) / (width * height);
	}
}

void Sprite::SetScreenPos(int _x, int _y)
{
	//set the screen local co ordinates
	local.x = _x;
	local.y = _y;
}

void Sprite::SetMapPos(int _x, int _y)
{
	//set global co ordinates
	global.x = _x;
	global.y = _y;
}

void Sprite::Move(int _x, int _y)
{
	//set global co ordinates
	global.x = _x;
	global.y = _y;
}

void Sprite::MoveDeltaX(int _x)
{
	//move a small distance
	global.x += _x;
	
	//check to see if global value is 
	//still in range
	if(global.x >= MAP_WIDTH)
		global.x = MAP_WIDTH;
	else if(global.x <= 0)
		global.x = 0;
}

void Sprite::MoveDeltaY(int _y)
{
	//move a small distance
	global.y += _y;

	//check to see if a global value
	//is still in range
	if(global.y >= MAP_HEIGHT)
		global.y = MAP_HEIGHT;
	else if(global.y <= 0)
		global.y = 0;
}

bool Sprite::CheckCollision(Sprite *object)
{
	//if there is already a collision, return true
	if(externalCollision)
		return true;

	//get x = x1 - x2, y = y1 - y2, r = r1 + r2
	int x = (global.x + (width / 2) + 1) - (object->GetGlobal().x + (object->GetWidth() / 2) + 1),
		y = (global.y + (height / 2) + 1) - (object->GetGlobal().y + (object->GetHeight() / 2) + 1),
		r = radius + object->GetRadius();

	if((x * x) + (y * y) < (r * r))
	{
		//check how far into edge 1 the collision is
		collision.x = global.x + width - object->GetGlobal().x;
		
		//check how far into edge 2 the collision is
		collisionSize.x = object->GetGlobal().x + object->GetWidth() - global.x;

		//check how far into edge 3 the collision is
		collision.y = global.y + height - object->GetGlobal().y;
		
		//check how far into edge 4 the collision is
		collisionSize.y = object->GetGlobal().y + object->GetHeight() - global.y;
		int root = sqrt((float)(x * x) + (y * y));
		int distance = abs(radius + object->GetRadius());
		distance -= root;
				
		//if more into left edge
		if(collisionSize.x < collision.x)
		{
			collisionSide = COLLISION_LEFT;
			collisionLeft = true;
			collisionSize.x = distance;
		}
	
		//if more into right edge
		if(collision.x < collisionSize.x)
		{
			collisionSide = COLLISION_RIGHT;
			collisionRight = true;
			collision.x = distance;
		}

		//if more into bot edge
		if(collisionSize.y < collision.y)
		{
			collisionSide = COLLISION_BOTTOM;
			collisionBot = true;
			collisionSize.y = distance;
		}

		//if more into top edge
		if(collision.y < collisionSize.y)
		{
			collisionSide = COLLISION_TOP;
			collisionTop = true;
			collision.y = distance;
		}
		
		return true;
	}

	return false;
}

bool Sprite::CheckCollision(Sprite *object, int _radius)
{
	//get x = x1 - x2, y = y1 - y2, r = r1 + r2
	int x = (global.x + (width / 2) + 1) - (object->GetGlobal().x + (object->GetWidth() / 2) + 1),
		y = (global.y + (height / 2) + 1) - (object->GetGlobal().y + (object->GetHeight() / 2) + 1),
		r = _radius + object->GetRadius();

	if((x * x) + (y * y) < (r * r))
	{
		//check how far into edge 1 the collision is
		collision.x = global.x + width - object->GetGlobal().x;
		
		//check how far into edge 2 the collision is
		collisionSize.x = object->GetGlobal().x + object->GetWidth() - global.x;

		//check how far into edge 3 the collision is
		collision.y = global.y + height - object->GetGlobal().y;
		
		//check how far into edge 4 the collision is
		collisionSize.y = object->GetGlobal().y + object->GetHeight() - global.y;

		//if more into left edge
		if(collisionSize.x < collision.x)
		{
			collisionSide = COLLISION_LEFT;
			collisionLeft = true;
		}
	
		//if more into right edge
		if(collision.x < collisionSize.x)
		{
			collisionSide = COLLISION_RIGHT;
			collisionRight = true;
		}

		//if more into bot edge
		if(collisionSize.y < collision.y)
		{
			collisionSide = COLLISION_BOTTOM;
			collisionBot = true;
		}

		//if more into top edge
		if(collision.y < collisionSize.y)
		{
			collisionSide = COLLISION_TOP;
			collisionTop = true;
		}
		
		return true;
	}

	return false;
}

bool Sprite::CheckCollisionAABB(Sprite *object)
{
    float halfw1 = width / 2.0f; 
    float halfh1 = height / 2.0f; 
	float halfw2 = object->GetWidth() / 2.0f; 
	float halfh2 = object->GetWidth() / 2.0f; 
        
	//if edge of one sprite is not overlapping
	//edge of another sprite there is no collision
	if((global.x + halfw1) < (object->GetGlobal().x - halfw2)) return false; 
	if((global.x - halfw1) > (object->GetGlobal().x + halfw2)) return false; 
	if((global.y + halfh1) < (object->GetGlobal().y - halfh2)) return false; 
	if((global.y - halfh1) > (object->GetGlobal().y + halfh2)) return false; 
   
    return true;
}

void Sprite::SetNoCollision()
{ 
	//reset all the colllisin parameters
	collisionSide = COLLISION_NONE;
	externalCollision = false;
	collisionSize.x = collisionSize.y = 0;
	collision.x = collision.y = 0;
}

Point Sprite::GetCollisionParam()
{
	//returns the depth of the collsion
	//by calculating from the collisin parameters
	Point temp;

	switch(collisionSide)
	{
	case COLLISION_TOP: 
		temp.x = collisionSize.x;
		temp.y = -collisionSize.y;
		break;

	case COLLISION_BOTTOM:
		temp.x = collision.x;
		temp.y = -collision.y;
		break;

	case COLLISION_LEFT:
		temp.x = collisionSize.x;
		temp.y = collisionSize.y;
		break;

	case COLLISION_RIGHT:
		temp.x = -collision.x;
		temp.y = collision.y;
		break;

	default:
		temp.x = 0;
		temp.y = 0;
		break;
	}

	//if the collision params are negative
	if((temp.x + global.x) < 0)
		temp.x *= -1;

	if((temp.y + global.y) < 0)
		temp.y *= -1;

	//or if the collision params are outside the map
	if((temp.x + global.x) > MAP_WIDTH)
	{
		temp.x = width - (MAP_WIDTH - global.x);
		temp.x *= -1;
	}

	if((temp.y + global.y) > MAP_HEIGHT)
	{
		temp.y = height - (MAP_HEIGHT - global.y);
		temp.y *= -1;
	}

	return temp;
}
