#include "Fighter.h"

Fighter::Fighter()
{
	//load the default bitmap and values
	Sprite::LoadSprite("Fighter.bmp");
	Sprite::SetTransColour(RGB(255, 0, 255));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(120);
	CSprite::SetFuel(30000);
	
	//set range for the bullet
	shot.SetRange(50);

	currentBase = -1;
	numberOfBases = 3;
}

Fighter::~Fighter()
{
	;
}

void Fighter::ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units)
{
	//if player wants to fire a bullet
	if(keyboard->GetKey('F') && isSelected())
	{
		Point temp, temp2;
		//get the point at which the bullet has to start
		temp.x = GetLocal().x + 50;
		temp.y = GetLocal().y + 50;
		
		temp2.x = GetGlobal().x + 50;
		temp2.y = GetGlobal().y + 50;
		shot.Fire(temp, mouse->GetCurrent(), temp2);
	}

	//if bullet is being rendered
	//check for collisions
	if(shot.GetRender() == true)
	{
		//check for collision only with enemy bases
		for(int i = 0; i < base.size(); ++i)
		{
			//if bullet hits the base
			if(shot.CheckCollision(base[i]) && base[i]->GetRender())
			{
				//take off health from the base
				base[i]->ChangeHealth(-8);
				//if base gets destroyed
				if(base[i]->GetHealth() <= 0 || base[i]->isDestroyed())
					currentBase = -1;
				
				//reset the bullet
				shot.Reset();
			}
		}

		shot.SetNoCollision();
	}
}

void Fighter::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	//if the number of bases is not same
	if(numberOfBases != base.size())
	{
		//update it
		numberOfBases = base.size();
		//search again for the closest base
		currentBase = -1;
		
		//reset the bullet
		shot.Reset();
		shot.SetNoCollision();
	}
	
	//if closest base has not been found
	if(currentBase == -1)
	{
		//if closest base has been found
		//which has been revealed by a seeker
		for(int i = 0; i < base.size(); ++i)
			if(base[i]->isDiscovered())
			{
				//update currentBase as the index number
				currentBase = i;
				this->ResetDestination();
				
				shot.Reset();
				shot.SetNoCollision();
				break;
			}
	}//if unit is at the closest base
	else if(this->isAtDestination() && currentBase != -1)
	{
		Point temp, temp2, temp3;
		//fire at it
		temp.x = 10;
		temp3.x = 5; 

		temp2.x = GetGlobal().x + 50;
		temp2.y = GetGlobal().y + 50;
		
		shot.Fire(temp, temp3, temp2);		
	}//if closest base has been found
	else if(currentBase != -1)
	{
		//move towards it
		Point temp;
		temp.x = base[currentBase]->GetGlobal().x + 850;
		temp.y = base[currentBase]->GetGlobal().y + 300;
		this->SetDestination(temp);
	}
}