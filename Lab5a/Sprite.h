#ifndef SPRITE_H
#define SPRITE_H

#include <Windows.h>
#include "Point.h"

//different render modes for the sprite
#define MODE_DEFAULT	1
#define MODE_TRANS		2

//if the sprite is colliding
#define COLLISION_NONE		0
#define COLLISION_RIGHT		11
#define COLLISION_LEFT		12
#define COLLISION_TOP		13
#define COLLISION_BOTTOM	14


class Sprite
{
	Point local;			//local screen co - ordinates
	Point global;			//global map co - ordinates
	Point spriteSheet;		//source spritesheet co - ordinates

	int width;				//width of sprite
	int height;				//height of sprite

	int radius;				//for collision detection

	int spriteWidth;		//width of sprite sheet
	int spriteHeight;		//height of spritesheet
	
	HBITMAP bitmap;			//bitmap for the sprite
	unsigned short RenderMode;	//for rendering transparent of with an alpha channel
	COLORREF trans;			//the colour to be transparent in the sprite
	bool render;			//if the sprite should be rendered

	int frameNo;			//the current frame of animation of the sprite
	bool scrolling;			//if the sprite uses scrolling

protected:
	int collisionSide;		//where the sprite is colliding
	bool collisionLeft, collisionRight, collisionTop, collisionBot;	//collision happening to the left , right, top or bottom of the sprite
	Point collision, collisionSize;	//for getting precise distance of collision

	bool externalCollision;	//if the sprite is colliding with an external unit

public:

	Sprite();
	~Sprite();

	bool LoadSprite(char *name);			//load the sprite bitmap from a file
	bool LoadSprite(char *name, int width, int height);		//load the spritesheet bitmap from a file

	void Render(HDC destHDC, HDC bitmapHDC);		//render the sprite to the buffer

	bool CheckCollision(Sprite *object);			//check for collision with another sprite
	bool CheckCollision(Sprite *object, int radius);//check for collision within a certain radius
	bool CheckCollisionAABB(Sprite *object);		//check for collision using axis aligned bounding box algorithm
	
	void Move(int _x, int _y);						//move the sprite to a position on the map
	void MoveDeltaX(int _x);						//move the sprite a small distance in the X direction
	void MoveDeltaY(int _y);						//move the sprite a small distance in the Y direction

	void SetScreenPos(int _x, int _y);				//set the X & Y of the sprite on the screen
	void SetMapPos(int _x, int _y);					//set the co-ordinates of the sprite on the map
	void SetWidthHeight(int w, int h) { height = h; width = w;}		//manually set width and height of the sprite

	void SetRenderMode(int _mode) { RenderMode = _mode;}	//set how you want to render the sprite
	void SetTransColour(COLORREF _colour){ trans = _colour;}	//set the transparency colour
	void SetRender(bool _render){ render = _render;}		//if you want to render the sprite

	void SetRadius(int _radius) { radius = _radius;}		//set the collision radius manually
	void SetNoCollision();							//reset all collision parameters
	void SetExternalCollision(bool _collision) { externalCollision = _collision;}	//if there is an external collision

	void SetScroll(bool _scroll) { scrolling = _scroll;}		//if the sprite has universal scrolling

	void NextFrame();								//next frame in the animation
	void PrevFrame();								//previous frame in the animation

	Point GetLocal() { return local;}				//get the screen co ordinates
	Point GetGlobal() { return global;}				//get the map co ordinates

	int GetWidth() { return width;}					//get the width of the sprite
	int GetHeight() { return height;}				//get the height of the sprite

	bool GetRender() { return render;}				//if the sprite is being rendered

	int GetRadius() { return radius;}				//get the collision radius of the sprite
	Point GetCollisionParam();						//get the collision depth of the sprite
};

#endif