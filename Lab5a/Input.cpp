#include "Input.h"
#include <math.h>

//set the singleton isntances to null
MouseButton *MouseButton::instance = nullptr;
KeyboardInput *KeyboardInput::instance = nullptr;

MouseButton::MouseButton()
{
	//initialise to default values
	OldPos.x = 0;
	OldPos.y = 0;

	CurrentPos.x = 0;
	CurrentPos.y = 0;
	
	//no buttons are being pressed
	buttons[3] = false;
	drag = false;
}

MouseButton::~MouseButton()
{
	delete instance;
}

KeyboardInput::KeyboardInput()
{
	//no key is being pressed
	for(int i = 0; i < 256; i++)
		keys[i] = false;
}

KeyboardInput::~KeyboardInput()
{
	delete instance;
}

void MouseButton::UpdateMousePos(int _x, int _y)
{
	CurrentPos.x = _x;
	CurrentPos.y = _y;
}

void MouseButton::SetOldPos()
{
	OldPos.x = CurrentPos.x;
	OldPos.y = CurrentPos.y;
}

void MouseButton::UpdateDragSpace()
{
	//mouse drag rectange width and height
	dragWidth = abs(CurrentPos.x - OldPos.x);
	dragHeight = abs(CurrentPos.y - OldPos.y);
}