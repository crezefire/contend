#ifndef TILE_H
#define TILE_H

#include <Windows.h>

//class to store the values of an individual Tile
class Tile
{
	HBITMAP bitmap;		//to store the bitmap of the tile
	
	int width;			//width and height of the tile
	int height;
	
public:
	Tile();
	~Tile();

	bool LoadTile(char *filename);			//load the tile
	void Render(HDC destHDC, HDC bitmapHDC, int xCoord, int yCoord);	//render the tile
	void Render(HDC destHDC, HDC bitmapHDC, int xCoord, int yCoord, int sourceX, int sourceY);

	int GetWidth() { return width;}			//get the width and height of the tile
	int GetHeight() { return height;}
};

#endif
