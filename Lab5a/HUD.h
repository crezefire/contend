#ifndef HUD_H
#define HUD_H

#include <Windows.h>
#include "CSprite.h"
#include "Font.h"

class Hud
{
	HBITMAP basicOverlay;	//bitmap for overlay
	HBITMAP miniMap;		//bitmap for minimap black background
	HBITMAP health;			//bitmap for the health bar
	HBITMAP fuel;			//bitmap for the fuel bar
	HBITMAP region;			//bitmap for the map region

	int height, width;		//width and height of the basicOverlay
public:
	Hud();
	~Hud();

	void Render(Base *base, CSprite *selected, bool multiple,int metal, HDC destHDC,HDC bitmapHDC);	//render the HUD
};

#endif