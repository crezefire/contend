#ifndef GRUNT_H
#define GRUNT_H

#include "CSprite.h"

class Grunt: public CSprite
{
	int currentUnit;	//stores the current unit it will follow
	int numberOfUnits;	//store the number of units
public:
	Grunt();
	~Grunt();

	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//process the AI for this unit

	int GetType() { return GRUNT;}
};

#endif