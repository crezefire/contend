#include <Windows.h>
#include "Tile.h"
#include "Point.h"

#ifndef BACKGROUND_H
#define BACKGROUND_H

#define SCREEN_WIDTH	1280
#define SCREEN_HEIGHT	760

#define MAX_TILES_X		30
#define MAX_TILES_Y		30

#define TILE_HEIGHT		250
#define TILE_WIDTH		250

#define MAP_WIDTH		MAX_TILES_X * TILE_WIDTH
#define MAP_HEIGHT		MAX_TILES_Y * TILE_HEIGHT

#define mainBackground	Background::GetInstance()

class Background
{
	static Background *instance;	//for singleton functionality

	Tile *allTiles;					//the different types of tiles that
	int BackgroundTiles[MAX_TILES_X][MAX_TILES_Y];	//the index number of the tiles

	Point scroll;					//the map scroll position

public:
	//function returns the current instance
	static Background *GetInstance()
    {
        //if instance is null
		if(!instance)
		{
			instance = new Background();
		}
        return instance;
    }

	Background();
	~Background();

	void LoadTileArray(int _maxTiles, ...);				//loads the tile array with different types of tiles
	void LoadBackgroundArray(int _tiles[MAX_TILES_X][MAX_TILES_Y]);		//loads the background array with the index number of the tiles
	void Render(HDC destHDC, HDC bitmapHDC);			//renders the background tiles
	void Reset(){ scroll.x = scroll.y = 0;}				//resets the scroll parameters
	void ScrollX(int _x);								//scrolls in the x direction
	void ScrollY(int _y);								//scrolls in the y direction

	Point GetScroll() { return scroll;}					//returns the scroll point
};
#endif