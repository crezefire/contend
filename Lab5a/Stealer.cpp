#include "Stealer.h"

Stealer::Stealer()
{
	Sprite::LoadSprite("Stealer.bmp");
	Sprite::SetTransColour(RGB(0, 0, 0));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(140);
	CSprite::SetFuel(35000);

	targetBase = -1;
}

Stealer::~Stealer()
{
	;
}

void Stealer::ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units)
{
	//if user presses F and there is a base nearby
	if(keyboard->GetKey('F') && isSelected())
	{
		for(int i = 0; i < base.size(); ++i)
			if(this->CheckCollision(base[i], 300))
			{
				//increase health of the base
				base[i]->ChangeHealth(70);
				
				//dsetroy the unit
				this->Destroy();
				break;
			}
		
		this->SetNoCollision();
	}
}

void Stealer::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	//if a base with low health has not been found
	if(targetBase == -1)
	{
		//find base with the lowest health
		for(int i = 0; i < base.size(); ++i)
		{
			if(base[i]->GetHealth() < base[i]->GetMaxHealth())
				targetBase = 1;
		}

	}//if reached the base with the lowest health
	else if(this->isAtDestination())
	{
		//change health of the base and suicide
		base[targetBase]->ChangeHealth(70);
		this->Destroy();

	}//if a base with low health has been found
	else if(targetBase != -1)
	{
		Point temp;
		temp.x = base[targetBase]->GetGlobal().x + 850;
		temp.y = base[targetBase]->GetGlobal().y + 200;
		this->SetDestination(temp);
	}

}