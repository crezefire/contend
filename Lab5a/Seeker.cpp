#include "Seeker.h"

Seeker::Seeker()
{
	Sprite::LoadSprite("Seeker.bmp");
	Sprite::SetTransColour(RGB(255, 0, 255));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(100);
	CSprite::SetFuel(100000);
	nextLocation.x = 7100;
	nextLocation.y = 7200;

	shifting = false;
}

Seeker::~Seeker()
{

}

void Seeker::ProcessPassive(std::vector<Base*> enemyBase, std::vector<CSprite*> units)
{
	//if the unit is colliding with an enemy base within a radius
	//detect it and render it
	for(int j = 0; j < enemyBase.size(); ++j)
	{
		if(this->CheckCollision(enemyBase[j], 400))
		{
			this->SetNoCollision();
			enemyBase[j]->SetRender(true);
			enemyBase[j]->Discovered();
		}
	}
}

void Seeker::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	//if unit is not moving towards the next location
	if(this->GetDestination().x != nextLocation.x && this->GetDestination().y != nextLocation.y)
	{
		//set the next location
		this->SetDestination(nextLocation);

	}//if the unit is at the destination and is not moving horizontally
	else if(this->isAtDestination() && !shifting)
	{
		//make it move horizontally
		nextLocation.x -= 250;
		this->SetDestination(nextLocation);
		shifting = true;

	}//if unit is moving horizontally and is at the next point
	else if(shifting && this->isAtDestination())
	{
		//reset the next point and make it move vertically
		if(nextLocation.y - 7000 >= 0)
			nextLocation.y -= 7000;
		else
			nextLocation.y += 7000;

		this->SetDestination(nextLocation);
		shifting = false;
	}
		
}

