#ifndef FONT_H
#define FONT_H

#include <Windows.h>

//easire to type in currentFont to get instance
#define currentFont Font::GetInstance()

class Font
{
	static Font *instance;		//instance for singleton functionality

	HBITMAP bitmap;				//bitmap to store the spritesheet
	COLORREF trans;				//the transparency colour of the bitmap

	int width, height;			//widtha and height of each character

public:
	static Font *GetInstance()		//function to return the instance
    {
        //if instance is null
		if(!instance)
		{
			instance = new Font();
		}
        return instance;
    }

	Font();
	~Font();

	void Print(char *str, int x, int y, HDC destHDC, HDC bitmapHDC);	//prints text on the screen
};

#endif