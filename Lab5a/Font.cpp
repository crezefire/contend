#include "Font.h"

Font *Font::instance = nullptr;

Font::Font()
{
	//load the file
	bitmap = (HBITMAP)LoadImage(NULL, "font.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	
	width = 32;
	height = 32;
	
	trans = RGB(0, 0, 0);
}

Font::~Font()
{
	;
}

void Font::Print(char *str, int x, int y, HDC destHDC, HDC bitmapHDC)
{
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, bitmap);
	
	//print each character on the string
	for(int i = 0; str[i] != '\0'; ++i)
	{
		//find the x and y values on the spritesheet
		int spriteX = (int)str[i]  % 16, spriteY = (int)str[i] / 16;
		
		TransparentBlt(destHDC, x + (i * 19), y, width, height, bitmapHDC, spriteX * 32, spriteY * 32, width, height, trans);
	}

	SelectObject(bitmapHDC, originalBitMap); 
}