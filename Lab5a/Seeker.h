#ifndef SEEKER_H
#define SEEKER_H

#include "CSprite.h"
#include "Bullet.h"

class Seeker : public CSprite
{
	Point nextLocation;	//the next location on the map to move to
	bool shifting;		//if it is shifting a column

public:
	Seeker();
	~Seeker();

	void ProcessPassive(std::vector<Base*> base, std::vector<CSprite*> units);			//detects enemy bases
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//processes the Ai for the unit

	int GetType(){ return SEEKER;}
};

#endif