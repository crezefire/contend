#ifndef CSPRITE_H
#define	CSPRITE_H

#include "Sprite.h"
#include "Input.h"
#include "Background.h"
#include "Font.h"
#include "Base.h"
#include "Bullet.h"
#include <vector>


//macros for different types of units
#define ENGINEER	30
#define FIGHTER		31
#define GRUNT		32
#define MINER		33
#define SEEKER		34
#define STEALER		35
#define WIZARD		36
#define NONE		37


class CSprite : public Sprite
{
	int currentHealth;		//current health of the unit
	int maxHealth;			//maximum health of the unit
	int currentFuel;		//current fuel of the unit
	int maxFuel;			//maximum fuel of the unit
		
	bool selected, releasedLeft, releasedRight;		//if unit has been selected
													//if mousebutton has been released
	Sprite border;			//the border if a unit has been selected
	Point destination;		//the point the unit has to move to
	bool reachedDestination;	//if the unit has reached the destination point
	bool move;				//if the unit has to move
	bool destroyed;			//if the unit has been destroyed i.e health <= 0

protected:
	Bullet shot;			//bullet used by certain units
	int baseNum;			//the base upgraded used by certain units

public:
	CSprite();
	~CSprite();

	void SetHealth(int _health) { currentHealth = maxHealth = _health;}		//set max and current health 
	void SetFuel(int _fuel) { currentFuel = maxFuel = _fuel;}				//sets max and current fuel
	void SetDestination(Point dest){ destination = dest; move = true; reachedDestination = false;}	//set a point to move to
	Point GetDestination(){ return destination;}		//returns the point the unit is moving to
	void ResetDestination(){ reachedDestination = false;}		//resets the value to false
	void MoveUnit();									//moves the unit

	void ChangeHealth(int _x);							//changes the health of the unit
	void ChangeFuel(int _x);							//changes the fuel of the unit
	
	void MouseInput();									//processes the selection and moving of the unit
	void ReleaseLeft(){ releasedLeft = true;}			//if left mouse button release has been registered

	bool isSelected() { return selected; }				//if the unit is selected
	void Select(){ selected = true;}					//select the unit
	
	void Render(HDC destHDC,HDC bitmapHDC);				//render the unit
	
	bool isDestroyed() { return destroyed;}				//if unit is destroyed
	void Destroy(){ destroyed = true;}					//destory the unit
	void ResetSprite();									//reset the unit if it is colliding with something

	virtual int GetType() = 0;							//get type of unit
	virtual void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal) = 0;	//process the AI of the unit
	virtual void ProcessPassive(std::vector<Base*> base, std::vector<CSprite*> units){};			//process the passive ability of the unit
	virtual void ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units){};				//process the active ability of the unit
	
	int GetHealth(){ return currentHealth;}				//returns the current health of unit
	int GetMaxHealth(){ return maxHealth;}				//returns the maximum health of unit
	int GetFuel() { return currentFuel;}				//returns the current fuel of unit
	int GetMaxFuel(){ return maxFuel;}					//returns the maximum fuel of unit
	bool isAtDestination(){ return reachedDestination;}	//if unit has reached the destination point
};

#endif