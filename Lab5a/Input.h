#ifndef INPUT_H
#define INPUT_H

#include "Point.h"

#define RMB		0	//Right Mouse Button
#define MMB		1	//Midle Mouse Button
#define LMB		2	//Left Mouse Button

//macros to get the instance of each class
#define mouse	MouseButton::GetInstance()
#define keyboard KeyboardInput::GetInstance()

//Mouse Button Class
class MouseButton
{
	static MouseButton *instance;	//instance for singleton functionality
	
	Point OldPos;					//the current and previous position of the mouse
	Point CurrentPos;
	
	bool buttons[3];				//if a button is being pressed
	bool drag;						//if the mouse is being dragged

	int dragWidth, dragHeight;		//mouse drag rectangle width and height

	void operator=(MouseButton const&); 

public:
	//function to return the instance of the class
	static MouseButton *GetInstance()
    {
        if(!instance)
		{
			instance = new MouseButton();
		}
        return instance;
    }
	
	MouseButton();
	~MouseButton();

	void SetMouseDown(int _button) { buttons[_button] = true;}		//to set the mouse button as pressed down
	void SetMouseUp(int _button) { buttons[_button] = false;}		//to set the mouse button as released
	void SetOldPos();												//to update the old position to match the current position of the mouse
	void SetMouseDrag(bool _drag) { drag = _drag;}					//if the mouse is being dragged or not
	
	bool GetMouseButton(int _button) {return buttons[_button];}		//to check if a mouse button is being pressed or not

	void UpdateMousePos(int _x, int _y);							//update the current X & Y postions of the mouse
	void UpdateDragSpace();											//update the width and height of the mouse drag rectangle
		
	bool isDrag() { return drag;}									//if mouse is being dragged

	int GetCurrentX() { return CurrentPos.x;}						//get X position
	int GetCurrentY() { return CurrentPos.y;}						//get Y position
	Point GetCurrent() { return CurrentPos;}						//get Current position of mouse in Point struct

	int GetOldX() { return OldPos.x;}								//get old X & Y position of the mouse
	int GetOldY() { return OldPos.y;}

	int GetDragWidth() { return dragWidth;}							//get drag width and height of the mouse
	int GetDragHeight() { return dragHeight;}
};


//Class for keyboard Input
class KeyboardInput
{
	static KeyboardInput *instance;	//instance for singleton functionality

	bool keys[256];					//to check if a key is being pressed

	void operator=(KeyboardInput const&); 

public:
	//function that returns the static instance
	static KeyboardInput *GetInstance()
    {
        //if instance is null
		if(!instance)
		{
			instance = new KeyboardInput();
		}
        return instance;
    }

	KeyboardInput();
	~KeyboardInput();
	
	void SetKeyDown(int i) { keys[i] = true;}		//set a key as being pressed
	void SetKeyUp(int i) { keys[i] = false;}		//set a key as released
	bool GetKey(int i) { return keys[i];}			//to check if a key is being pressed
};

#endif