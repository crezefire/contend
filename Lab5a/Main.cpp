/*****************************
Vimarsh Raina
Student ID: 1100317
Aim: To write a 2D game
which uses the windows API
*****************************/

#include <windows.h>
#include <WinGDI.h>
#include <stdio.h>
#include <mmsystem.h>
#include "Game.h"


//variable of the game class
//used to interact with the game
Game contend;

HWND        ghwnd;

LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);


void RegisterMyWindow(HINSTANCE hInstance)
{
    WNDCLASSEX  wcex;									

    wcex.cbSize        = sizeof (wcex);				
    wcex.style         = CS_HREDRAW | CS_VREDRAW;		
    wcex.lpfnWndProc   = WndProc;						
    wcex.cbClsExtra    = 0;								
    wcex.cbWndExtra    = 0;								
    wcex.hInstance     = hInstance;						
    wcex.hIcon         = 0; 
    wcex.hCursor       = LoadCursor (NULL, IDC_ARROW);	
															
    wcex.hbrBackground = (HBRUSH) (COLOR_WINDOW+1);
    wcex.lpszMenuName  = NULL;							
    wcex.lpszClassName = "FirstWindowClass";				
    wcex.hIconSm       = 0; 

	RegisterClassEx (&wcex);							
}


BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow)
{
	HWND        hwnd;
	hwnd = CreateWindow ("FirstWindowClass",					
						 "Contend",		  	
						 WS_SYSMENU,	
						 0,			
						 0,			
						 SCREEN_WIDTH,			
						 SCREEN_HEIGHT,			
						 NULL,					
						 NULL,					
						 hInstance,				
						 NULL);								
	if (!hwnd)
	{
		return FALSE;
	}

    ShowWindow (hwnd, nCmdShow);						
    UpdateWindow (hwnd);	
	ghwnd = hwnd;
	return TRUE;

}


	
LRESULT CALLBACK WndProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)											
    {														
		//sets the mousebutton or keyboard to
		//true of false depending on
		//value passed
		case WM_CREATE:	
			break;

		case WM_SIZE:
			break;	

		case WM_LBUTTONDOWN:
			mouse->SetMouseDown(LMB);
			break;
		
		case WM_LBUTTONUP:
			mouse->SetMouseUp(LMB);
			//if the user is dragging the mouse
			//select multiple units
			if(mouse->isDrag())
				contend.SelectUnits();
			//user has stopped dragging
			mouse->SetMouseDrag(false);
			break;

		case WM_RBUTTONDOWN:
			mouse->SetMouseDown(RMB);
			break;

		case WM_RBUTTONUP:
			mouse->SetMouseUp(RMB);
			break;

		case WM_MBUTTONDOWN:
			mouse->SetMouseDown(MMB);
			break;

		case WM_MBUTTONUP:
			mouse->SetMouseUp(MMB);
			break;

		case WM_KEYDOWN:
			keyboard->SetKeyDown(wParam);
			break;

		case WM_KEYUP:
			keyboard->SetKeyUp(wParam);
			break;

		case WM_MOUSEMOVE:
			//if the left mouse has been pressed and the the user is moving the mouse
			//and if the mouse is not already being dragged
			if(mouse->GetMouseButton(LMB) == true && mouse->isDrag() == false)
			{
				//set the mouse drag to true
				//update the mouse positions
				mouse->SetMouseDrag(true);
				mouse->SetOldPos();
				mouse->UpdateMousePos(LOWORD (lParam), HIWORD (lParam));
				//update the drag rectangle created by the mouse
				mouse->UpdateDragSpace();
			}
			else if(mouse->isDrag() == true)
			{
				//if mouse is already being dragged
				//update the new mouse position & rectangle
				mouse->UpdateMousePos(LOWORD (lParam), HIWORD (lParam));
				mouse->UpdateDragSpace();
			}
			else
				mouse->UpdateMousePos(LOWORD (lParam), HIWORD (lParam));
			break;

		case WM_PAINT:
			 break;		

		case WM_DESTROY:	
			PostQuitMessage(0);	
			break;				
	}													

	return DefWindowProc (hwnd, message, wParam, lParam);		
															
}



int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int nCmdShow)			
{									
    MSG         msg;	
	HDC	hdcWindow;

	//stores the current state of the program
	//for use in the stae machine
	int programState = GAME_MENU;

	RegisterMyWindow(hInstance);

   	if (!InitialiseMyWindow(hInstance, nCmdShow))
		return FALSE;
	
	//set the double buffers
	contend.SetBuffers(ghwnd); 
	//initialise the new values
	//set the map and units
	contend.InitialisePlayers();

	while (TRUE)					
    {							
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
		{
		    if (msg.message==WM_QUIT)
				break;
			TranslateMessage (&msg);							
			DispatchMessage (&msg);
		}

		else
		{		
			//switches & executes based on the
			//current state of the program
			switch(programState)
			{
			case GAME_RUN:
				programState = contend.GameRun();
				break;

			case GAME_MENU:
				programState = contend.GameMenu();
				break;

			case GAME_PAUSE:
				programState = contend.GamePause();
				break;

			case GAME_END:
				programState = contend.GameEnd();
				break;
			}

		}
    }

    //cleanup and deallocate
	contend.ReleaseResources(ghwnd);
	return msg.wParam ;										
}



