#include "Base.h"

Base::Base(): Sprite()
{
	//set default values of the base
	health = 400;
	maxHealth = 400;
	timer = 0;

	//load the bitmap for the base from the file
	Sprite::LoadSprite("BaseSheet2.bmp", 750, 750);
	Sprite::SetRenderMode(MODE_TRANS);
	Sprite::SetTransColour(RGB(255, 0, 255));
	Sprite::SetRadius(375);

	selected = false;
	releasedLeft = true;
	destroyed = false;
	upgraded = false;
	timerBegin = false;
	discovered = false;
	type = 37;
}

Base::~Base()
{
	;
}

void Base::MouseInput()
{
	//if left mouse button is pressed
	if(mouse->GetMouseButton(LMB))
	{
		releasedLeft = false;
		
	}
	
	//if left mouse button had been pressed and now has been released
	if(!mouse->GetMouseButton(LMB) && releasedLeft == false)
	{
		releasedLeft = true;
		
		//if the mouse position is inside the base sprite, the base has been selected
		if(mouse->GetCurrentX() > GetLocal().x && mouse->GetCurrentX() < (GetLocal().x + GetWidth() )&&
		mouse->GetCurrentY() > GetLocal().y && mouse->GetCurrentY() < (GetLocal().y + GetHeight()))
		{
			selected = true;
		}
		else
			selected = false;

	}

}

void Base::ChangeHealth(int change)
{
	
	//change health
	health += change;

	//check to see if the value is valid
	if(health >= maxHealth)
		health = maxHealth;
	else if(health <= 0)
	{
		//if health is less than 0, the base has been destoryed
		destroyed = true;
		SetRender(false);
		health = 0;
	}
}

void Base::Render(HDC destHDC,HDC bitmapHDC)
{
	//process the mouse input
	MouseInput();

	//render the base sprite
	Sprite::Render(destHDC,bitmapHDC);
}