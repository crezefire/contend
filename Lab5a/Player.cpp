#include "Player.h"

Player::Player()
{
	//sets the default metal amount
	metal = 500;

	//currently no bases or units selected
	currentlySelected = nullptr;
	currentBase = nullptr;
	multiple = false;
	minerMetal = false;

	//load the bitmap for enemy and friendly units for minimap
	greenDot = (HBITMAP)LoadImage(NULL, "mini_friendly.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	redDot = (HBITMAP)LoadImage(NULL, "mini_enemy.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
}

Player::~Player()
{
	for(unsigned int i = 0; i < units.size(); ++i)
		delete units[i];

	for(unsigned int i = 0; i < bases.size(); ++i)
		delete bases[i];
}

void Player::AddEngineer(int x, int y)
{
	//declare a new unit and add it to the vector
	Engineer *temp = new Engineer();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddSeeker(int x, int y)
{
	//declare a new unit and add it to the vector
	Seeker *temp = new Seeker();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddFighter(int x, int y)
{
	//declare a new unit and add it to the vector
	Fighter *temp = new Fighter();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddStealer(int x, int y)
{
	//declare a new unit and add it to the vector
	Stealer *temp = new Stealer();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddGrunt(int x, int y)
{
	//declare a new unit and add it to the vector
	Grunt *temp = new Grunt();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddWizard(int x, int y)
{
	//declare a new unit and add it to the vector
	Wizard *temp = new Wizard();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddMiner(int x, int y)
{
	//declare a new unit and add it to the vector
	Miner *temp = new Miner();
	
	temp->SetMapPos(x, y);

	units.push_back(temp);
}

void Player::AddBase(int x, int y)
{
	//declare a new base and add it to the vector
	Base *temp = new Base();

	temp->SetMapPos(x, y);
	
	bases.push_back(temp);
}

void Player::SetBases()
{
	//checks to see if any two bases are colliding
	for(int i = 0; i < bases.size(); ++i)
		for(int j = 0; j < bases.size(); ++j)
		{
			if(i == j)
				continue;

			//if they are colliding
			while(bases[i]->CheckCollision(bases[j]))
			{
				//move one of them a certain distance away
				bases[i]->MoveDeltaX(bases[i]->GetCollisionParam().x);
				bases[i]->MoveDeltaY(bases[i]->GetCollisionParam().y);
			}
		}
}

void Player::HideBases()
{
	//set render for all the bases as false
	//so that they don't render
	for(int i = 0; i < bases.size(); ++i)
		bases[i]->SetRender(false);
}

void Player::UpdateSelected()
{
	//counter to see if multiple units have been selected
	int counter = 0;
	
	//loops through all units to check if a unit has been selected
	for(int i = 0; i < units.size(); ++i)
	{
		if(units[i]->isSelected())
		{
			currentlySelected = units[i];
			++counter;
		}
	}

	//if no units have been selected
	if(counter == 0)
	{
		currentlySelected = nullptr;
	}

	//if one unit has been selected
	if(counter == 1)
		multiple = false;

	currentBase = nullptr;
	
	//to check if a base has been selected
	for(int i = 0; i < bases.size(); ++i)
	{
		if(bases[i]->isSelected())
			currentBase = bases[i];
	}
}


void Player::CheckCollisions()
{
	//checks for collision between each and every unit
	for(int i = 0; i < units.size(); ++i)
		for(int j = 0; j < units.size(); ++j)
		{
			//if the units are the same
			if(j == i)
				continue;

			units[i]->CheckCollision(units[j]);
		}
}

void Player::CheckCollisions(CSprite *temp, int i)
{
	//checks collision between a unit and
	//an external unit
	//if there is a collision is sets it as true
	if(units[i]->CheckCollision(temp))
		units[i]->SetExternalCollision(true);
	else
		units[i]->SetExternalCollision(false);
}

void Player::CheckCollisions(CSprite *temp, int i, int radius)
{
	//checks collision between a unit and
	//an external unit within a certain radius
	//it is processing the fog of war
	//if there is a collision is sets it as true
	if(units[i]->CheckCollision(temp, radius))
		 if(units[i]->GetRender() == false)
			units[i]->SetRender(true);
}

void Player::CheckCollisionWithBase()
{
	//checks for collision of every unit with every base
	for(int i = 0; i < units.size(); ++i)
		for(int j = 0; j < bases.size(); ++j)
			if(bases[j]->GetRender())
				units[i]->CheckCollision(bases[j]);
}

void Player::CheckCollisionWithBase(std::vector<Base*> enemyBases)
{
	//checks for collision of every unit with every enemy base
	for(int i = 0; i < units.size(); ++i)
		for(int j = 0; j < enemyBases.size(); ++j)
			if(enemyBases[j]->GetRender())
				units[i]->CheckCollision(enemyBases[j]);
}

void Player::ResetCollisions()
{
	//sets collision parameters for all units as false
	for(int i = 0; i < units.size(); ++i)
		units[i]->SetNoCollision();
}

void Player::ResetRender()
{
	//set all units to not render
	for(int i = 0; i < units.size(); ++i)
		units[i]->SetRender(false);
}

void Player::SelectUnits(int width, int height, Point a, Point c)
{
	//selects units within a certain area
	//process the mouse drag rectangle
	int scrollX = mainBackground->GetScroll().x;
	int scrollY = mainBackground->GetScroll().y;

	//create a temporary sprite with parameters
	//given by the mouse drag rectangle
	Sprite *temp = new Sprite();

	//sets the values
	temp->SetMapPos(a.x + scrollX, a.y + scrollY);
	temp->SetWidthHeight(width, height);
	
	int counter = 0;
	
	//then checks for collision between the
	//temporary sprite and a unit
	for(int i = 0; i < units.size(); ++i)
	{
		if(units[i]->CheckCollisionAABB(temp))
		{
			//if there is a collision
			//select the unit
			units[i]->ReleaseLeft();
			units[i]->Select();
			
			//update the currently selected unit
			currentlySelected = units[i];
			++counter;
		}
	}

	//if more than one unit is selected
	if(counter > 1)
	{
		multiple = true;
	}
	else if(counter == 0)
	{
		multiple = false;
		currentlySelected = nullptr;
	}
	else
		multiple = false;
	
	delete temp;
}

void Player::DrawMiniMapFriendly(HDC destHDC, HDC bitmapHDC)
{
	HBITMAP originalBitMap;
	//loads the friendly bitmap
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, greenDot);
	
	//gets each unit and scales it down, 25 in the X and 45 in the Y direction
	//then renders the unit onto the minimap
	for(int i = 0; i < units.size(); ++i)
	{
		StretchBlt(destHDC, (units[i]->GetGlobal().x / 25) + 990, (units[i]->GetGlobal().y / 45) + 570, 4, 
			2, bitmapHDC, 0, 0, 4, 2, SRCCOPY);
	}

	//does the same for each base
	for(int i = 0; i < bases.size(); ++i)
	{
		StretchBlt(destHDC, (bases[i]->GetGlobal().x / 25) + 990, (bases[i]->GetGlobal().y / 45) + 570, 30, 
			15, bitmapHDC, 0, 0, 4, 2, SRCCOPY);
	}

	SelectObject(bitmapHDC, originalBitMap);

}

void Player::DrawMiniMapEnemy(HDC destHDC, HDC bitmapHDC)
{
	HBITMAP originalBitMap;
	//loads the enemy bitmap
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, redDot);
	
	//gets each unit and scales it down, 25 in the X and 45 in the Y direction
	//then renders the unit onto the minimap
	for(int i = 0; i < units.size(); ++i)
	{
		//only draws it if the unit is being rendered on the map
		if(units[i]->GetRender())
			StretchBlt(destHDC, (units[i]->GetGlobal().x / 25) + 990, (units[i]->GetGlobal().y / 45) + 570, 4, 
			2, bitmapHDC, 0, 0, 4, 2, SRCCOPY);
	}

	for(int i = 0; i < bases.size(); ++i)
	{
		//only draws it if the unit is being rendered on the map
		if(bases[i]->GetRender())
			StretchBlt(destHDC, (bases[i]->GetGlobal().x / 25) + 990, (bases[i]->GetGlobal().y / 45) + 570, 30, 
			15, bitmapHDC, 0, 0, 4, 2, SRCCOPY);
	}

	SelectObject(bitmapHDC, originalBitMap);

}

void Player::ProcessPassive(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits)
{
	//goes through each unit and process the passive
	//depending on the type of unit, either the enemy base vector the current base vector is passed
	for(int i = 0; i < units.size(); ++i)
	{
		switch(units[i]->GetType())
		{
		case SEEKER:
			units[i]->ProcessPassive(enemyBase, units);
			units[i]->SetNoCollision();
			break;

		case ENGINEER:
			units[i]->ProcessPassive(bases, units);
			break;

		case MINER:
			//checks to see if a miner is closed to a base
			//then gives the player bonus metal
			for(int j = 0; j < bases.size(); ++j)
				if(units[i]->CheckCollision(bases[j], 250) && !bases[j]->isDestroyed())
				{
					minerMetal = true;
					units[i]->SetNoCollision();
					break;
				}
				else
					minerMetal = false;
				
			break;
		}
	}
}

void Player::ProcessActive(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits)
{
	//goes through each unit and process the active abilities
	//depending on the type of unit, either the enemy base vector the current base vector is passed
	for(int i = 0; i < units.size(); ++i)
	{
		switch(units[i]->GetType())
		{
		case FIGHTER:
			units[i]->ProcessActive(enemyBase, units);
			break;

		case ENGINEER:
			//engineer active can only be processed if there is 500 metal
			if(metal >= 500)
				units[i]->ProcessActive(bases, units);
			break;

		case WIZARD:
			units[i]->ProcessActive(bases, enemyUnits);	
			break;

		case STEALER:
			units[i]->ProcessActive(bases, units);	
			break;
		}
	}

	//check to see if any bases have been upgraded
	for(int i = 0; i < bases.size(); ++i)
	{
		if(bases[i]->isUpgraded() && metal >= 500)
		{
			//downgrades the base and starts the timer
			bases[i]->Downgrade();
			bases[i]->StartTimer();
			//reduces the metal the player has
			metal -= 500;
		}
		else
			bases[i]->Downgrade();
	}
}

void Player::ProcessAI(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits)
{
	//goes through each unit and process the AI
	//depending on the type of unit, either the enemy base vector the current base vector is passed
	for(int i = 0; i < units.size(); ++i)
	{
		switch(units[i]->GetType())
		{
		case FIGHTER:
			units[i]->ProcessAI(enemyBase, units, metal);
			break;

		case ENGINEER:
			units[i]->ProcessAI(bases, units, metal);
			break;

		case WIZARD:
			units[i]->ProcessAI(bases, enemyUnits, metal);	
			break;

		case STEALER:
			units[i]->ProcessAI(bases, units, metal);	
			break;

		case GRUNT:
			units[i]->ProcessAI(bases, units, metal);	
			break;

		case SEEKER:
			units[i]->ProcessAI(bases, units, metal);	
			break;

		case MINER:
			units[i]->ProcessAI(bases, units, metal);	
			break;
		}
	}
}

void Player::MoveUnits()
{
	//calls the move function for each unit
	for(int i = 0; i < units.size(); ++i)
		units[i]->MoveUnit();
}

void Player::ProcessInput()
{
	//processes the mouse input for each unit
	//also if the player preses R, the sprite resets
	for(int i = 0; i < units.size(); ++i)
	{
		units[i]->MouseInput();
		if(units[i]->isSelected() && keyboard->GetKey('R'))
			units[i]->ResetSprite();
	}
}

void Player::IncrementTimer()
{
	//goes through all bases
	//if their spawn timer is running it increments it
	for(int i = 0; i < bases.size(); ++i)
	{
		//check to see if timer is running and
		//increases timer
		if(bases[i]->TimerRunning())
			bases[i]->IncreaseTime();

		//if timer has reacehd maximum range
		//time to spawn a new unit
		if(bases[i]->GetTimer() >= 30)
		{
			//creates a new random unit
			//for testing only
			CSprite *temp = new Seeker();
			
			//radius of collision
			int radius = 375 + 50 + 50;
			bool collision = false, out = false;
			
			//this checks for a spot around the base
			//where the unit can spawn without colliding with any other unit
			for(int x =  -radius; x <= radius; x++)
			{
				for(int y = -radius; y <= radius; y++)
				{
					//if found a spot around the base
					if (((x * x) + (y * y)) == (radius * radius))
					{
						//move the temp sprite to the location
						temp->SetMapPos(x + bases[i]->GetGlobal().x + 325, y + bases[i]->GetGlobal().y + 325);
						
						//check to see if it collides with any other unit
						for(int i = 0; i < units.size(); ++i)
							if(temp->CheckCollision(units[i]))
								collision = true;
						
						//if there is a collision
						//continue the loop
						//else break the loop
						if(collision)
						{
							collision = false;
							continue;
						}
						else
						{
							out = true;
							break;
						}
					}//end of if(x * x)...
				}//end of inner for loop

				//if broken out of inner loop
				//break out of outer loop
				if(out == true)
					break;
			
			}//end of outer for loop

			//since a collision free point has been found
			int x = temp->GetGlobal().x, y = temp->GetGlobal().y;

			//add a unit to the vector
			//depeding on the type on unit the base has
			switch(bases[i]->GetType())
			{
			case SEEKER:
				AddSeeker(x, y);
				break;

			case STEALER:
				AddStealer(x, y);
				break;

			case WIZARD:
				AddWizard(x, y);
				break;

			case MINER:
				AddMiner(x, y);
				break;

			case FIGHTER:
				AddFighter(x, y);
				break;

			case ENGINEER:
				AddEngineer(x, y);
				break;

			case GRUNT:
				AddGrunt(x, y);
				break;

			case NONE:
				//stop and reset timer
				bases[i]->StopTimer();
				bases[i]->ResetTimer();
				break;
			}//end of switch

			//stop and reset timer
			bases[i]->StopTimer();
			bases[i]->ResetTimer();
			
			//delete temporary variable created earlier
			delete temp;

			//set the type of unit the base spawns as none
			bases[i]->SetType(NONE);
		
		}//end of if(base timer > ......
	
	}//end of for loop
}

void Player::ResetPlayer()
{
	//remove all elements from both
	//the vectors by clearing them
	units.clear();
	bases.clear();

	//set the default start values
	metal = 500;
	minerMetal = false;

	currentlySelected = nullptr;
	currentBase = nullptr;
	multiple = false;
}

void Player::Render(HDC destHDC, HDC bitmapHDC)
{
	//if a unit or base has been destroyed
	int toDelete = -1;
	
	//renders all units
	for(int i = 0; i < units.size(); ++i)
	{
		units[i]->Render(destHDC, bitmapHDC);

		if(units[i]->isDestroyed())
			toDelete = i;
	}

	//remove a unit from the vector
	if(toDelete != -1)
	{
		//declares the iterator for the unit
		std::vector<CSprite*>::iterator i = units.begin();
		units.erase(i + toDelete);
	}

	toDelete = -1;
	
	//renders and goes to the next frame of the animation
	//of each base
	for(int i = 0; i < bases.size(); ++i)
	{
		bases[i]->NextFrame();
		bases[i]->Render(destHDC, bitmapHDC);
		if(bases[i]->isDestroyed())
			toDelete = i;
	}

	//remove a base from the vector
	if(toDelete != -1)
	{
		//iterator for the base
		std::vector<Base*>::iterator i = bases.begin();
		bases.erase(i + toDelete);
	}

}