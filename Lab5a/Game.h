#ifndef GAME_H
#define GAME_H

#include <stdio.h>
#include <ctime>

#include "Point.h"
#include "Sprite.h"
#include "Input.h"
#include "Tile.h"
#include "Background.h"
#include "Seeker.h"
#include "CSprite.h"
#include "Bullet.h"
#include "Player.h"
#include "HUD.h"
#include "Font.h"

//the number of pixels to scroll
#define SCROLL_VALUE	10

//metal gained per frame
#define METAL_INCREMENT	4

//macros for the state machine
#define GAME_RUN	20
#define GAME_PAUSE	21
#define GAME_MENU	22
#define GAME_END	23

class Game
{
	int ticker;			//used for controlling frame rate
	int gameEndTimer;	//to wait a few seconds when game has ended
	HBITMAP	mousespace;	//the bitmap for the rectangle created when dragging the mouse
	Sprite	menuScreen, menuPlay, menuExit;	//sprites for the menu
	Point a, c;			//the co ordinates of the opposite ends of a diagonal of the rectangle of the mouse drag

	HBITMAP		theOldFrontBitMap, theOldBackBitMap;
	RECT		screenRect;
	HDC			backHDC, frontHDC, bitmapHDC;	//Hardware device contexts for the Buffers

	Sprite cursor;	//the custon cursor bitmap
	
	Player computer, user;	//the two players

	Hud mainDisplay;	//the Heads Up Display
	bool keydown;		//to check if a key has been pressed
	bool victory;		//if the player won or lost

public:
	Game();
	~Game();
	void InitialisePlayers();	//initialises players to default values

	void DrawMouseDrag();		//to draw the mouse drag rectangle

	int GameRun();				//run the game, process the values and render
	int GameMenu();				//show the menu
	int GamePause();			//only render the game, and show pause message
	int GameEnd();				//to show if player has won or lost
	void GameReset();			//reset all values to default


	BOOL WaitFor(unsigned long delay);	//to add a delay to control frame rate

	void SetBuffers(HWND ghwnd);
	void DisplayFrame();
	void ReleaseResources(HWND ghwnd);

	void SelectUnits();		//to select multiple units if the user drags the mouse
	void FogOfWar();		//process the fog of war for the player

	void CheckInterCollisions();	//check collisions between the player and user
};

#endif