#ifndef ENGINEER_H
#define ENGINEER_H

#include "CSprite.h"

class Engineer: public CSprite
{
	int closestBase;	//the index number of the closest base
	bool atBase;		//if engineer is near a base
		
	int unitSpawnOrder[8];	//the order in which it should spawn units
	
	int currentUnit;		//the current type of unit
	int numberOfBases;		//total number of bases the player has

public:
	Engineer();
	~Engineer();

	void ProcessPassive(std::vector<Base*> base, std::vector<CSprite*> units);		//to process the passive ability
	void ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units);		//to process the active ability
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//to process the AI

	int GetType() { return ENGINEER;}
};

#endif