#ifndef BASE_H
#define BASE_H

#include "Sprite.h"
#include "Input.h"

#define BASE_HEIGHT		750
#define BASE_WIDTH		750

#define MAX_BASES		3

class Base : public Sprite
{
	int health;			//health of the base
	int maxHealth;		//max health of the base
	int type;			//the type of unit the base is going to spawn
	int timer;			//the timer before the base spawns a unit

	bool selected;		//if the base is selected
	bool releasedLeft;	//if mouse button has been release
	bool destroyed;		//if base has been destroyed
	bool upgraded;		//if base has been upgraded
	bool timerBegin;	//if the base timer has been running
	bool discovered;	//if base has been discovered by the enemy

public:
	void MouseInput();							//process the mouse input to select the base
	bool isSelected() { return selected;}		//if the base has been selected
	bool isDestroyed() { return destroyed;}		//if the base has been destoryed
	bool isUpgraded() { return upgraded;}		//to check if base has been upgraded
	bool isDiscovered() { return discovered;}	//to check if base has been discovered by enemy

	void ChangeHealth(int change);				//change health of the base
	int GetHealth(){ return health;}			//get current health of the base
	int GetMaxHealth(){ return maxHealth;}		//get maximum health of the base

	void Render(HDC destHDC,HDC bitmapHDC);		//render the base

	int GetType() {return type;}				//get the type of units the base holds
	void SetType(int _type){ type = _type;}		//set the type of units the base spwans
	void Upgrade(){ upgraded = true;}			//upgrade the base
	void Downgrade(){ upgraded = false;}		//downgrade the base
	void Discovered(){ discovered = true;}		//base discovered by enemies

	void StartTimer(){ timerBegin = true;}		//start timer before unit spawns
	void StopTimer(){ timerBegin = false;}		//stop timer
	bool TimerRunning(){ return timerBegin;}	//top check if timer is running
	void ResetTimer(){ timer = 0;}				//reset timer for base
	void IncreaseTime() { ++timer;}				//increase the timer for unit spawn time
	int GetTimer() { return timer;}				//check the timer for unit spawn time

	Base();
	~Base();
};

#endif