#ifndef FIGHTER_H
#define FIGHTER_H

#include "CSprite.h"

class Fighter: public CSprite
{
	int currentBase;	//to get the current base the AI is processing
	int numberOfBases;	//number of bases the player has

public:
	Fighter();
	~Fighter();
		
	void ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units);		//process the active ability
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);//process the AI of the unit

	int GetType() { return FIGHTER;}
};
#endif