#ifndef STEALER_H
#define STEALER_H

#include "CSprite.h"
//#include "Base.h"

class Stealer: public CSprite
{
	int targetBase;	//the base with the lowest health
public:
	Stealer();
	~Stealer();
	
	void ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units);		//to die while increasing the health of a nearby allied base
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//process the Ai for the unit

	int GetType(){ return STEALER;}
};

#endif