#ifndef MINER_H
#define MINER_H

#include "CSprite.h"

class Miner : public CSprite
{
	int closestBase;	//index number of the closest base
	int numberOfBases;	//total number of bases on the map

public:
	Miner();
	~Miner();
	
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//process the AI for the unit

	int GetType(){ return MINER;}
};

#endif