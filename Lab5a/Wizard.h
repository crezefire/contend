#ifndef WIZARD_H
#define WIZARD_H

#include "CSprite.h"

class Wizard: public CSprite
{
public:
	Wizard();
	~Wizard();

	void ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units);		//passive is that it can attack enemy units
	void ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal);	//process AI for this unit

	int GetType(){ return WIZARD;}		//returns type of unit
};

#endif