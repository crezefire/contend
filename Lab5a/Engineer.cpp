#include "Engineer.h"

Engineer::Engineer()
{
	//load the default sprite
	//set the values
	Sprite::LoadSprite("Engineer.bmp");
	Sprite::SetTransColour(RGB(255, 0, 255));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(90);
	CSprite::SetFuel(30000);

	closestBase = -1;
	atBase = false;
	numberOfBases = 3;

	unitSpawnOrder[0] = FIGHTER;
	unitSpawnOrder[1] = MINER;
	unitSpawnOrder[2] = STEALER;
	unitSpawnOrder[3] = FIGHTER;
	unitSpawnOrder[4] = GRUNT;
	unitSpawnOrder[5] = ENGINEER;
	unitSpawnOrder[6] = FIGHTER;
	unitSpawnOrder[7] = SEEKER;

	currentUnit = 0;
}

Engineer::~Engineer()
{
	;
}

void Engineer::ProcessPassive(std::vector<Base*> base, std::vector<CSprite*> units)
{
	//to check and see if there are any units units near it
	for(int i = 0; i < units.size(); ++i)
	{
		if(this->GetLocal().x == units[i]->GetLocal().x &&
			this->GetLocal().y  == units[i]->GetLocal().y)
			continue;
		
		//if there is a unit in range
		//keep increasing the fuel of that unit
		if(this->CheckCollision(units[i], 300))
		{
			units[i]->ChangeFuel(1);
			this->SetNoCollision();
		}
	}
}

void Engineer::ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units)
{
	//if user presses F to upgrade a base
	if(keyboard->GetKey('F') && isSelected())
	{
		//check if a base is close it it
		for(int i = 0; i < base.size(); ++i)
			if(this->CheckCollision(base[i], 300))
			{
				//store the index number of that base
				baseNum = i;
				break;
			}
			this->SetNoCollision();
	}

	//if index number is assigned
	if(baseNum != -1)
	{
		//and player presses a number key to upgrade to 
		//a certain type of unit
		if(keyboard->GetKey('1'))
		{
			base[baseNum]->SetType(ENGINEER);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('2'))
		{
			base[baseNum]->SetType(FIGHTER);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('3'))
		{
			base[baseNum]->SetType(GRUNT);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('4'))
		{
			base[baseNum]->SetType(MINER);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('5'))
		{
			base[baseNum]->SetType(SEEKER);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('6'))
		{
			base[baseNum]->SetType(STEALER);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('7'))
		{
			base[baseNum]->SetType(WIZARD);
			base[baseNum]->Upgrade();
			baseNum = -1;
		}
		else if(keyboard->GetKey('8'))
		{
			base[baseNum]->SetType(NONE);
			baseNum = -1;
		}
	}
}

void Engineer::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	Point temp;
	
	//if the number of bases has changed
	if(numberOfBases != base.size())
	{
		//set the new number
		numberOfBases = base.size();
		//re assess the closest base
		closestBase = -1;
	}
	
	//if closestBase not found
	if(closestBase == -1)
	{
		temp.x = 0;
		temp.y = 0;

		//find it and give it the index value
		for(int i = 0; i < base.size(); ++i)
			if(temp.x < base[i]->GetGlobal().x)
			{
				temp.x = base[i]->GetGlobal().x;
				closestBase = i;
			}
	}//if unit is at a destination close to a base
	else if(this->isAtDestination() && closestBase != -1)
	{
		//if the player metal is more than 500 and 
		//the closest base is not spawning any unit
		if(metal >= 500 && !base[closestBase]->TimerRunning())
		{
			//spawn a unit
			base[closestBase]->SetType(unitSpawnOrder[currentUnit]);
			base[closestBase]->Upgrade();
			
			//move to the next type of unit
			++currentUnit;
			
			//reset if it goes over
			if(currentUnit >= 7)
				currentUnit = 0;
		}
	}
	else if(closestBase != -1)
	{
		//if closest base has been found ,move towards it
		temp.x = base[closestBase]->GetGlobal().x + 850;
		temp.y = base[closestBase]->GetGlobal().y;
		this->SetDestination(temp);
	}
	
}