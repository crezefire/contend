#include "Wizard.h"

Wizard::Wizard()
{
	//set the default values, load the bitmap from file
	Sprite::LoadSprite("Wizard.bmp");
	Sprite::SetTransColour(RGB(255, 0, 255));
	Sprite::SetRenderMode(MODE_TRANS);

	CSprite::SetHealth(170);
	CSprite::SetFuel(40000);

	//set the range of the bullet
	shot.SetRange(70);
}

Wizard::~Wizard()
{
	;
}

void Wizard::ProcessActive(std::vector<Base*> base, std::vector<CSprite*> units)
{
	//if player wants to attack an enemy unit	
	if(keyboard->GetKey('F') && isSelected())
	{
		Point temp, temp2;
		//fire the bullet to side where the cursor is
		temp.x = GetLocal().x + 50;
		temp.y = GetLocal().y + 50;
		
		temp2.x = GetGlobal().x + 50;
		temp2.y = GetGlobal().y + 50;
		
		//fire the shot
		shot.Fire(temp, mouse->GetCurrent(), temp2);
	}

	//if bullet is being fired
	if(shot.GetRender() == true)
	{
		//check to see if it collides with any enemy unit
		for(int i = 0; i < units.size(); ++i)
		{
			//if there is a collision, reduce health of affected unit
			if(shot.CheckCollision(units[i]) && units[i]->GetRender())
			{
				units[i]->ChangeHealth(-5);
				//reset the bullet
				shot.Reset();
			}
		}

		shot.SetNoCollision();
	}
}

void Wizard::ProcessAI(std::vector<Base*> base, std::vector<CSprite*> units, int metal)
{
	//the wizard has not AI
	//this is to give advantage to the player
	//since the computer units do not collide with bases and act faster
}