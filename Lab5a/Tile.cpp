#include "Tile.h"
#include <Windows.h>

Tile::Tile()
{

}

Tile::~Tile()
{

}

bool Tile::LoadTile(char *fileName)
{
	//load the bitmap file into the bitmap container
	bitmap = (HBITMAP)LoadImage(NULL, fileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	if(!bitmap)
		return false;

	BITMAP temp;
	GetObject(bitmap, sizeof(temp), &temp);
	
	//get width and height of the bitmap
	height = temp.bmHeight;
	width = temp.bmWidth;

	return true;
}

void Tile::Render(HDC destHDC, HDC bitmapHDC, int xCoord, int yCoord)
{
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

	//render the sprite onto the screen
	BitBlt(destHDC, xCoord, yCoord, xCoord + width, yCoord + height, bitmapHDC, 0, 0, SRCCOPY);
			
	SelectObject(bitmapHDC, originalBitMap);
}

void Tile::Render(HDC destHDC, HDC bitmapHDC, int xCoord, int yCoord, int sourceX, int sourceY)
{
	HBITMAP originalBitMap;
	originalBitMap = (HBITMAP)SelectObject(bitmapHDC, bitmap);

	//render the sprite to the screen except in this case the x & y values of the sprite sheet have shifted
	BitBlt(destHDC, xCoord, yCoord, xCoord + width, yCoord + height, bitmapHDC, sourceX, sourceY, SRCCOPY);
			
	SelectObject(bitmapHDC, originalBitMap);
}