#include "CSprite.h"
#include <math.h>

#define FUEL_REDUCTION	-3


CSprite::CSprite()
{
	//set the default values for
	//all the members
	currentHealth = 0;
	maxHealth = 0;
	currentFuel = 0;
	maxFuel = 0;
	
	selected = false;
	releasedLeft = true;
	destroyed = false;
	reachedDestination = false;
	baseNum = -1;
	
	//load the selection border
	border.LoadSprite("Selection.bmp");
	border.SetRenderMode(MODE_TRANS);
	border.SetTransColour(RGB(255, 0, 255));
	
	destination.x = 0;
	destination.y = 0;

	move = false;
}

CSprite::~CSprite()
{

}

int CSprite::GetType()
{
	return NONE;
}

void CSprite::MoveUnit()
{
	//if move is true and the unit has fuel
	if(move && GetFuel() > 0)
	{
		//if there is a collision
		if(collisionSide != COLLISION_NONE)
		{
			//check if unit can move on x
			if (GetGlobal().x < destination.x && collisionRight == false)
			{
				MoveDeltaX(1);
				ChangeFuel(FUEL_REDUCTION);
			}
			else if (GetGlobal().x > destination.x && collisionLeft == false)
			{	
				ChangeFuel(FUEL_REDUCTION);
				MoveDeltaX(-1);
			}
			
			//check if unit can move on y
			if (GetGlobal().y  < destination.y && collisionTop == false)
			{
				MoveDeltaY(1);
				ChangeFuel(FUEL_REDUCTION);
			}
			else if (GetGlobal().y  >destination.y && collisionBot == false)
			{
				MoveDeltaY(-1);
				ChangeFuel(FUEL_REDUCTION);
			}
		}
		else
		{
			//if no collision
			//if destination is to the left or right
			if (GetGlobal().x < destination.x)
			{
				MoveDeltaX(1);
				ChangeFuel(FUEL_REDUCTION);
			}
			else if (GetGlobal().x > destination.x)
			{
				MoveDeltaX(-1);
				ChangeFuel(FUEL_REDUCTION);
			}

			//if destination is up or down
			if (GetGlobal().y < destination.y)
			{
				MoveDeltaY(1);
				ChangeFuel(FUEL_REDUCTION);
			}
			else if (GetGlobal().y > destination.y)
			{
				MoveDeltaY(-1);
				ChangeFuel(FUEL_REDUCTION);
			}
		}
		
		Sprite::SetNoCollision();
		
		//if you have reached near the destination point
		int x = GetGlobal().x - destination.x , y = GetGlobal().y - destination.y ;
		if((x * x) + (y * y) < 100)
		{
			move = false;
			reachedDestination = true;
		}
	}
}

void CSprite::Render(HDC destHDC, HDC bitmapHDC)
{
	//render the sprite
	Sprite::Render(destHDC, bitmapHDC);
	
	//render the bullet
	shot.Render(destHDC, bitmapHDC);

	//if unit is selected render the selection border
	if(selected)
	{
		border.Move(GetGlobal().x, GetGlobal().y);
		border.Render(destHDC, bitmapHDC);
	}
}

void CSprite::ChangeHealth(int _x)
{
	//change and check the current health
	currentHealth += _x;
		
	if(currentHealth >= maxHealth)
		currentHealth = maxHealth;
		
	if(currentHealth <= 0)
	{
		destroyed = true;
		currentHealth = 0;
	}
}

void CSprite::ChangeFuel(int _x)
{
	//change and check the current fuel
	currentFuel += _x;

	if(currentFuel >= maxFuel)
		currentFuel = maxFuel;

	if(currentFuel <= 0)
		currentFuel = 0;
}

void CSprite::ResetSprite()
{
	//get the collision parameter to see
	//how much the sprite is colliding by
	//then move the sprite by the collision depth
	this->MoveDeltaX(this->GetCollisionParam().x );
	this->MoveDeltaY(this->GetCollisionParam().y );
	move = false;
}

void CSprite::MouseInput()
{
	//if left mouse button has been pressed
	//register it
	if(mouse->GetMouseButton(LMB))
	{
		releasedLeft = false;
		
	}
	
	//if let mouse button was pressed and has now been released
	if(!mouse->GetMouseButton(LMB) && releasedLeft == false)
	{
		releasedLeft = true;
		
		//check to see if the point of release lies within the sprite
		//if it does select it
		if(mouse->GetCurrentX() > GetLocal().x && mouse->GetCurrentX() < (GetLocal().x + GetWidth() )&&
		mouse->GetCurrentY() > GetLocal().y && mouse->GetCurrentY() < (GetLocal().y + GetHeight()))
		{
			selected = true;
		}
		else
			selected = false;

	}
	
	//if right mousebutton has been pressed
	if(mouse->GetMouseButton(RMB))
	{
		releasedRight = false;
	}

	//if right mouse button was pressed and has been released
	if(!mouse->GetMouseButton(RMB) && releasedRight == false)
	{
		releasedRight = true;
		
		//if sprite is selected
		if(selected)
		{
			int tempScroll = 0;
			
			//get the point the sprite has to move to
			destination.x = mouse->GetCurrentX() + mainBackground->GetScroll().x;
			destination.y = mouse->GetCurrentY() + mainBackground->GetScroll().y;
			
			//if the point is outside the map i.e. on the HUD
			if(destination.y < MAP_HEIGHT)
				destination.y = mouse->GetCurrentY() + mainBackground->GetScroll().y;
			else
				destination.y = mouse->GetCurrentY() + mainBackground->GetScroll().y - 175;

			//if the point will make ths sprite go off the map
			if( (destination.y + GetHeight()) > MAP_HEIGHT)
				destination.y -= GetHeight();

			if( (destination.x + GetWidth()) > MAP_WIDTH)
				destination.x -= GetWidth();

			move = true;

		}
	}
}