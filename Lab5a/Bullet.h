#ifndef BULLET_H
#define BULLET_H

#include "Sprite.h"

class Bullet : public Sprite
{
	Point velocity;		//velocity of the bullet
	int range, distanceTravelled;	//range of the bullet and the distance travelled

public:
	Bullet();
	~Bullet();

	void SetRange(int _range){range = _range;}		//set the maximum range the bullet can travel
	void Fire(Point from, Point to, Point Start);	//to fire the bullet in a certain direction
	void Move();									//move the bullet
	void Reset();									//reset the bullet

	void Render(HDC destHDC,HDC bitmapHDC);			//render the bullet
};

#endif