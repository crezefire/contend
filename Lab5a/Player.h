#ifndef PLAYER_H
#define PLAYER_H

#include <vector>
#include "Seeker.h"
#include "Engineer.h"
#include "Fighter.h"
#include "Stealer.h"
#include "Grunt.h"
#include "Wizard.h"
#include "Miner.h"
#include "Base.h"

class Player
{
	HBITMAP greenDot;	//red square for rendering friendly units on mini map
	HBITMAP redDot;		//red square for rendering enemy units on mini map

	std::vector<CSprite*> units;	//vector of all the different types of units
	std::vector<Base*> bases;		//vector of all the bases the player has

	int metal;			//amount of metal the player has
	bool minerMetal;	//if the player has a miner, the player gets more metal

public:
	CSprite *currentlySelected;		//currently selected unit by the user
	Base *currentBase;				//currently selected base by the user
	bool multiple;					//if user has selectd multiple units

	Player();
	~Player();

	void AddSeeker(int x, int y);		//adds a seeker to the vector
	void AddEngineer(int x, int y);
	void AddFighter(int x, int y);
	void AddStealer(int x, int y);
	void AddGrunt(int x, int y);
	void AddWizard(int x, int y);
	void AddMiner(int x, int y);

	void AddBase(int x, int y);			//adds a base to the base vector
	void SetBases();					//checks and resets the base two bases are colliding
	std::vector<Base*> GetBaseVector() { return bases;}		//give the base vector
	std::vector<CSprite*> GetUnitsVector() { return units;}	//gives the units vector
	void HideBases();					//all bases now have render set as false
	
	void SelectUnits(int width, int height, Point a, Point c);	//function to select multiple units
	void CheckCollisions();										//checks for collision between the units
	void CheckCollisions(CSprite *temp, int i);					//checks for collision between an internal and external unit
	void CheckCollisions(CSprite *temp, int i, int radius);		//checks for collision between an internal and external unit within a radius
	void CheckCollisionWithBase();								//checks for collision between units and bases
	void CheckCollisionWithBase(std::vector<Base*> enemyBases);	//checks for collision betwwen units and enemy bases
	void ResetCollisions();										//sets all units as not colliding
	void ResetRender();											//sets all units to render false
	void ResetPlayer();											//removes all elements from both the vectors and resets the metal

	void Render(HDC destHDC, HDC bitmapHDC);					//render all valid units
	void DrawMiniMapFriendly(HDC destHDC, HDC bitmapHDC);		//draws the position of units and bases as friendly
	void DrawMiniMapEnemy(HDC destHDC, HDC bitmapHDC);			//draws the position of units and bases as enemies

	void UpdateSelected();										//updates the currently selected unit and/or base
	void ProcessPassive(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits);	//processes the passive ability of each unit
	void ProcessActive(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits);		//processes the active ability of each unit
	void ProcessAI(std::vector<Base*> enemyBase, std::vector<CSprite*> enemyUnits);			//processes the AI of each unit
	void ProcessInput();										//processes the mouse input for each of the units
	void MoveUnits();											//moves the units if they have to
	void IncrementTimer();										//increases the spawn timer on the bases
	void ChangeMetal(int _m){ metal += _m; if(minerMetal) ++metal;}	//changes the metal for the player
	
	int GetTotalUnits(){ return units.size();}					//get the total number of units
	CSprite *GetUnit(int i){ return units[i];}					//get a sinlge unit
	int GetMetal(){ return metal;}								//get the amount of metal the player has
	int GetNumberOfBases(){ return bases.size();}				//get the total number of bases standing for the player

};

#endif
